package ut.com.atlassian.applinks;

import org.junit.Test;
import com.atlassian.applinks.MyPluginComponent;
import com.atlassian.applinks.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}