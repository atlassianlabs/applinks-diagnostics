package com.atlassian.applinks;

public interface MyPluginComponent
{
    String getName();
}