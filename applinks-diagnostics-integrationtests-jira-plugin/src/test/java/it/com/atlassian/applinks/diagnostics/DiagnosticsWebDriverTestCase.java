package it.com.atlassian.applinks.diagnostics;


import com.atlassian.jira.nimblefunctests.framework.NimbleFuncTestCase;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnvironmentBasedProductInstance;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.google.common.base.Objects;
import it.com.atlassian.applinks.diagnostics.util.QuickAuth;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

/**
 * The base class for Projects Web Driver tests.
 *
 * @since v5.2
 */
public abstract class DiagnosticsWebDriverTestCase extends NimbleFuncTestCase
{
    private static final Dimension BROWSER_SIZE = new Dimension(1280, 1024);

    /**
     * Indicates whether the #setUpOnce method has run for this class yet.
     */
    private static boolean IS_SETUP_ONCE = false;

    protected Backdoor backdoor;
    protected JiraTestedProduct product = new JiraTestedProduct(new EnvironmentBasedProductInstance());
    protected PageBinder pageBinder;

    @Override
    public void beforeMethod()
    {
        super.beforeMethod();
        backdoor = new Backdoor(environmentData);

        final AtlassianWebDriver webDriver = product.getTester().getDriver();
        webDriver.manage().window().setSize(BROWSER_SIZE);
        if (!IS_SETUP_ONCE)
        {
            setUpOnce();
            IS_SETUP_ONCE = true;
        }
        pageBinder = product.getPageBinder();

        setUpTest();
    }

    // Stub implementation, suitable for overriding
    protected void setUpTest()
    { }

    /**
     * Override this to run set up code ONCE for the given test case. This is best used in conjunction with {@code
     */
    protected void setUpOnce()
    { }

    @Override
    public void afterMethod()
    {
        super.afterMethod();
    }

    /**
     * Returns the logged in user
     *
     * @return the logged in user
     */
    private String getLoggedInUser()
    {
        // down-casting here to avoid deprecation warnings on AtlassianWebDriver (see SELENIUM-187)
        JavascriptExecutor driver = (JavascriptExecutor) openAboutPage();

        return (String) driver.executeScript("AJS && AJS.Meta && AJS.Meta.get('remote-user')");
    }

    final protected void login(String username, String password)
    {
        // skip login/logout dance if we're already logged in as the right user
        String loggedInUser = getLoggedInUser();
        if (!Objects.equal(username, loggedInUser))
        {
            // we may want to get a bit smarter about unnecessary logout/login
            QuickAuth.forProduct(product).logout().login(username, password);
        }

        // reload the page
        openAboutPage();
    }

    private WebDriver openAboutPage()
    {
        WebDriverTester tester = product.getTester();
        WebDriver driver = tester.getDriver();
        JIRAEnvironmentData envData = product.environmentData();

        // any page will do, this one should be quick to render...
        product.getTester().gotoUrl(envData.getBaseUrl() + "/secure/AboutPage.jspa");

        return driver;
    }
}