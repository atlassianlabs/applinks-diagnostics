package it.com.atlassian.applinks.diagnostics.util;

import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.page.AdminHomePage;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * Home administration page, which is synonym for projects page.
 *
 * @since 4.4
 */
public class JiraAdminAddons extends AbstractJiraAdminPage
{
    private final static String URI = "/plugins/servlet/upm/marketplace/featured";

    @Inject
    protected PageElementFinder elementFinder;

    @ElementBy(cssSelector = "#content")
    private PageElement contentSection;

    public String getUrl()
    {
        return URI;
    }

    @Override
    public TimedCondition isAt()
    {
        return contentSection.timed().isPresent();
    }

    @Override
    public String linkId()
    {
        return "admin_summary";
    }

    public PageElement getApplicationLinksDiagnosticsLink(){
        return elementFinder.find(By.id("diagnose-application-links"));
    }
}
