package it.com.atlassian.applinks.diagnostics;

import com.atlassian.applinks.pageobjects.AppLinksDiagnosticsPage;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.pageobjects.elements.PageElement;
import it.com.atlassian.applinks.diagnostics.util.JiraAdminAddons;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
@RestoreOnce("JIRA-with-sysadmin.xml")
public class DiagnosticsAccessTestsAsAdmin extends DiagnosticsAccessTest {

    @Override
    protected void login() {
        login("admin", "admin");
    }

    @Override
    protected void assertLink(JiraAdminAddons adminAddonsPage) {
        PageElement diagnosticsLink = adminAddonsPage.getApplicationLinksDiagnosticsLink();
        assertFalse("Diagnostics link exists", diagnosticsLink.isPresent());
    }

    @Override
    protected void assertAppLinksPage(AppLinksDiagnosticsPage diagnosticsPage) {
        assertFalse(diagnosticsPage.isAt());
    }
}
