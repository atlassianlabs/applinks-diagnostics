package it.com.atlassian.applinks.diagnostics;

import com.atlassian.applinks.pageobjects.AppLinksDiagnosticsPage;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.pageobjects.elements.PageElement;
import it.com.atlassian.applinks.diagnostics.util.JiraAdminAddons;
import org.junit.Test;
import static junit.framework.TestCase.assertTrue;

@WebTest({Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
@RestoreOnce("JIRA-with-sysadmin.xml")
public class DiagnosticsAccessTestsAsSysadmin extends DiagnosticsAccessTest {

    @Override
    protected void login() {
        login("sysadmin", "sysadmin");
    }

    @Override
    protected void assertLink(JiraAdminAddons adminAddonsPage) {
        PageElement diagnosticsLink = adminAddonsPage.getApplicationLinksDiagnosticsLink();
        assertTrue("Diagnostics link exists", diagnosticsLink.isPresent());
        assertTrue("Diagnostics link is visible", diagnosticsLink.isVisible());
    }

    @Override
    protected void assertAppLinksPage(AppLinksDiagnosticsPage diagnosticsPage) {
        assertTrue(diagnosticsPage.isAt());
    }

    @Test
    public void testDiagnosticsLinksGoesToDiagnosticsPage()
    {
        login();
        JiraAdminAddons adminAddons = pageBinder.navigateToAndBind(JiraAdminAddons.class);
        adminAddons.getApplicationLinksDiagnosticsLink().click();

        assertTrue("JIRA is at diagnostics page", product.isAt(AppLinksDiagnosticsPage.class));
    }
}
