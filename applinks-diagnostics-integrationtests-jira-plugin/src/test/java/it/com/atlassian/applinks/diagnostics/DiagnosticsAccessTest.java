package it.com.atlassian.applinks.diagnostics;

import com.atlassian.applinks.pageobjects.AppLinksDiagnosticsPage;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.pageobjects.elements.PageElement;
import it.com.atlassian.applinks.diagnostics.util.JiraAdminAddons;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;

/**
 * @since v6.0
 */
public abstract class DiagnosticsAccessTest extends DiagnosticsWebDriverTestCase {

    protected abstract void login();
    protected abstract void assertLink(JiraAdminAddons adminAddonsPage);
    protected abstract void assertAppLinksPage(AppLinksDiagnosticsPage diagnosticsPage);

    @Test
    public void testDiagnosticsLinksIsPresentInAdminPage()
    {
        login();
        JiraAdminAddons adminAddons = pageBinder.navigateToAndBind(JiraAdminAddons.class);
        assertLink(adminAddons);
    }

    @Test
    public void testCanNavigateToAppLinksDiagnosticsPageURL()
    {
        login();
        AppLinksDiagnosticsPage diagnosticsPage = product.goTo(AppLinksDiagnosticsPage.class);
        assertAppLinksPage(diagnosticsPage);
    }
}
