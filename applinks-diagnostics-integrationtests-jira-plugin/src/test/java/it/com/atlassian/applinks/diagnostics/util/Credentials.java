package it.com.atlassian.applinks.diagnostics.util;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Credentials sent to JIRA.
 */
@JsonSerialize
class Credentials
{
    public String username;
    public String password;

    Credentials()
    {
    }

    public Credentials(String username, String password)
    {
        this.username = username;
        this.password = password;
    }
}