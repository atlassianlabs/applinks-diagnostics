applinks-diagnostics
====================

Here we hope lays the solution to reducing app links related support issues!

Getting started
===============

Execute the following in the repository parent directory, for example `~/src/atlassian/applinks-diagnostics`:

    mvn3 clean install -DskipTests

JIRA
----

JIRA's the mother ship, the control center, the command center, the mother land, cap com, vladimir putin, you get the
idea... It's has the UI you use to debug stuff.

1. Now the JAR has been created, we need to access the `~/src/atlassian/applinks-diagnostics/applinks-diagnostics-plugin` subdirectory:

        cd applinks-diagnostics-plugin

2. Startup JIRA:

        ATLAS_MVN=`which mvn3` atlas-debug --product jira --version 6.3-SNAPSHOT

3. A sucessful startup will display the following:

	    [INFO] jira started successfully in 397s at http://<hostname>:5990/jira

2. Browse to [http://localhost:5990/jira/plugins/servlet/applinksDiagnostics/ui](http://localhost:5990/jira/plugins/servlet/applinksDiagnostics/ui?os_username=admin&os_password=admin)

Making Changes
--------------

After making changes, use `atlas-cli` and `pi` to install the new version:

1. Go to the `~/src/atlassian/applinks-diagnostics/applinks-diagnostics-plugin` subdirectory:

2. Startup the Atlassian CLI:

	    ATLAS_MVN=`which mvn3` atlas-cli --context-path /jira

3. Use `pi` to install the new version:

	    ...
	    maven2> pi
	    ...
	    [INFO] Install Plugin: Uploading 'applinks-diagnostics-4.0.2-SNAPSHOT.jar' to server via UPM: http://localhost:5990/jira
	    [INFO] Current project: applinks-diagnostics

Confluence
----------

To use Confluence, change the product and version command line arguments for `atlas-debug`.

Troubleshooting
=============

If the following error occurs it's likely you're not in the correct directory (e.g.: `~/src/atlassian/applinks-diagnostics/applinks-diagnostics`).
To fix this go to that directory.

		[ERROR] Failed to execute goal com.atlassian.maven.plugins:maven-amps-dispatcher-plugin:4.2.20:cli (default-cli) on project applinks-diagnostics-parent: Couldn't detect an AMPS product to dispatch to -> [Help 1]


Notes on the available diagnosis
=============================

There are 11 diagnostic tasks defined in the plugin, which tests or validates various application links aspects. 

The diagnostic tasks classes can be located in com.atlassian.applinks.diagnostics.tasks package.

The active tasks are:
* HttpConnectivityRpcUrlDiagnosticTask: HTTP connectivity diagnosis to verify remote application RPC URL availability
* HttpConnectivityDisplayUrlDiagnosticTask: Simple HTTP connectivity diagnosis
* HttpConnectivityManifestRpcUrlDiagnosticTask: HTTP connectivity diagnosis to retrieve remote application applink manifest
* HttpEchoDiagnosticTask: HTTP echo diagnosis to verify if the remote application has applink diagnostic plugin installed, and its version
* AuthenticationDiagnosticTask: Authentication diagnosis to validate the authentication result against the remote application
* ValidateRemoteManifestTask: Retrieve and validate the remote application applink manifest
* EntityLinkConsistencyDiagnosticTask: Validate the entitylink data

And the following tasks are currently disabled:
* ValidateRemoteConfluenceRPCActiveTask
* ValidateThreeLeggedOAuthConfigurationTask
* ValidateTwoLeggedOAuthConfigurationTask
* ValidateTwoLeggedImpersonationOAuthConfigurationTask

The diagnostic tasks are defined in the plugin descriptor (atlassian-plugin.xml) under the diagnostic-task module