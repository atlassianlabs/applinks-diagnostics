package com.atlassian.applinks.abinitio.rest;

import com.atlassian.plugins.rest.common.Link;
import com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "applicationId")
public class ApplicationIdEntity extends AbstractEntity {

    @XmlElement(name = "id")
    private String id;

    /**
     * Default constructor needed for serialization
     */
    public ApplicationIdEntity()
    {

    }

    public ApplicationIdEntity(final String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder {
        private String id;
        private List<Link> links = Lists.newArrayList();

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public ApplicationIdEntity build() {
            ApplicationIdEntity applicationIdEntity = new ApplicationIdEntity(this.id);
            applicationIdEntity.setLinks(this.links);
            return applicationIdEntity;
        }

        public Builder link(Link link) {
            this.links.add(link);
            return this;
        }
    }
}
