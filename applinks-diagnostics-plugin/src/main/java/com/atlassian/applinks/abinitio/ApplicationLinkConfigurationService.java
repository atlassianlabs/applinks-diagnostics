package com.atlassian.applinks.abinitio;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.core.property.ApplicationLinkProperties;

/**
 * Describes basic functionality for interacting with Application Link data.
 */
public interface ApplicationLinkConfigurationService {
    public Iterable<ApplicationId> getApplicationIds();

    public Iterable<PropertyServiceApplicationLinkConfigurationService.ApplicationLinkEntry> getApplicationLinks();

    ApplicationLinkProperties getApplicationLinkById(String applicationId);
}
