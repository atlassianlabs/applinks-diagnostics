package com.atlassian.applinks.abinitio.rest;

import com.atlassian.plugins.rest.common.Link;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractEntity {
    @XmlElement(name="link")
    private List<Link> links;

    public void setLinks(final List<Link> links)
    {
        this.links = links;
    }

    public void addLink(Link link)
    {
        if(links==null)
            links = new ArrayList<Link>();
        links.add(link);
    }
}
