package com.atlassian.applinks.abinitio.rest;

import com.atlassian.applinks.abinitio.ApplicationLinkConfigurationService;
import com.atlassian.applinks.abinitio.PropertyServiceApplicationLinkConfigurationService;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.core.property.ApplicationLinkProperties;
import com.atlassian.applinks.diagnostics.rest.DiagnoseApplicationLinkResource;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.plugins.rest.common.Link;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.sun.jersey.spi.resource.Singleton;

import javax.annotation.Nullable;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

@Path(AbinitioApplicationLinkResource.ABINITIO_APPLICATION_LINK_CONTEXT)
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Singleton
public class AbinitioApplicationLinkResource {

    public static final String ABINITIO_APPLICATION_LINK_CONTEXT = "abinitioApplicationLink";
    private final ApplicationLinkConfigurationService abinitioApplicationLinkService;
    private final ApplicationProperties applicationProperties;
    private final TypeAccessor typeAccessor;
    private final I18nResolver i18nResolver;

    public AbinitioApplicationLinkResource(final ApplicationLinkConfigurationService abinitioApplicationLinkService, final ApplicationProperties applicationProperties, final TypeAccessor typeAccessor, final I18nResolver i18nResolver) {

        this.abinitioApplicationLinkService = abinitioApplicationLinkService;
        this.applicationProperties = applicationProperties;
        this.typeAccessor = typeAccessor;
        this.i18nResolver = i18nResolver;
    }

    @GET
    @Path("applicationId")
    public Response applicationIds() throws URISyntaxException {
        return Response.ok(ApplicationIdEntities.builder().ids(getApplicationIdEntities()).link(Link.self(new URI(this.getRestBaseUrl() + AbinitioApplicationLinkResource.ABINITIO_APPLICATION_LINK_CONTEXT))).build()).build();
    }

    @GET
    @Path("applicationId/list")
    public Response listApplicationIds() throws URISyntaxException {
        return Response.ok(getApplicationIdEntities()).build();
    }

    @GET
    @Path("applicationLink/{applicationId}")
    public Response applink(@PathParam("applicationId") final String applicationId) throws URISyntaxException {

        return Response.ok(getApplicationLinkEntity(applicationId)).build();
    }

    @GET
    @Path("applicationLink/list")
    public Response listApplicationLinks() throws URISyntaxException {
        return Response.ok(getApplicationLinkEntities()).build();
    }

    private ApplicationLinkEntity getApplicationLinkEntity(final String applicationId) throws URISyntaxException {
        ApplicationLinkProperties applicationLinkProperties = this.abinitioApplicationLinkService.getApplicationLinkById(applicationId);
        final ApplicationType applicationType = typeAccessor.loadApplicationType(applicationLinkProperties.getType());
        return ApplicationLinkEntity.builder()
                .applicationId(applicationId)
                .rpcUrl(applicationLinkProperties.getRpcUrl().toASCIIString())
                .displayUrl(applicationLinkProperties.getDisplayUrl().toASCIIString())
                .type(applicationLinkProperties.getType().get())
                .name(applicationLinkProperties.getName())
                .system(applicationLinkProperties.isSystem())
                .primary(applicationLinkProperties.isPrimary())
                .iconUrl(applicationType != null ? applicationType.getIconUrl().toASCIIString() : "")
                .link(Link.self(new URI(this.getRestBaseUrl() + AbinitioApplicationLinkResource.ABINITIO_APPLICATION_LINK_CONTEXT))).build();
    }

    private Iterable<ApplicationIdEntity> getApplicationIdEntities() {
        final String restUrl = this.getRestBaseUrl();

        return Iterables.transform(abinitioApplicationLinkService.getApplicationIds(), new Function<ApplicationId, ApplicationIdEntity>() {
            @Override
            public ApplicationIdEntity apply(@Nullable ApplicationId applicationId) {
                Link applicationLink = null;
                try {
                    applicationLink = Link.link(new URI(restUrl + AbinitioApplicationLinkResource.ABINITIO_APPLICATION_LINK_CONTEXT + "/applicationLink/" + applicationId.get()), "applicationLink");
                } catch (URISyntaxException e) {

                }
                Link diagnose = null;
                try {
                    diagnose = Link.link(new URI(restUrl + DiagnoseApplicationLinkResource.DIAGNOSE_APPLICATION_LINK_CONTEXT + "/" + applicationId.get()), "diagnose");
                } catch (URISyntaxException e) {

                }
                return ApplicationIdEntity.builder()
                        .link(applicationLink)
                        .link(diagnose)
                        .id(applicationId.get())
                        .build();
            }
        });
    }

    private Iterable<ApplicationLinkEntity> getApplicationLinkEntities() {
        return Iterables.transform(abinitioApplicationLinkService.getApplicationLinks(), new Function<PropertyServiceApplicationLinkConfigurationService.ApplicationLinkEntry, ApplicationLinkEntity>()
        {
            @Override
            public ApplicationLinkEntity apply(@Nullable PropertyServiceApplicationLinkConfigurationService.ApplicationLinkEntry input)
            {
                ApplicationId applicationId = input.getApplicationId();
                ApplicationLinkProperties applicationLinkProperties = input.getApplicationLinkProperties();
                final ApplicationType applicationType = typeAccessor.loadApplicationType(applicationLinkProperties.getType());
                return ApplicationLinkEntity.builder()
                        .applicationId(applicationId.get())
                        .rpcUrl(applicationLinkProperties.getRpcUrl().toASCIIString())
                        .displayUrl(applicationLinkProperties.getDisplayUrl().toASCIIString())
                        .iconUrl(applicationType != null ? applicationType.getIconUrl().toASCIIString() : "")
                        .type(i18nResolver.getText(applicationType.getI18nKey()))
                        .name(applicationLinkProperties.getName())
                        .build();
            }
        });
    }

    private String getRestBaseUrl() {
        final String baseUrl = this.applicationProperties.getBaseUrl(UrlMode.ABSOLUTE);
        return baseUrl + "/rest/applinksdiagnostics/1.0/";
    }
}
