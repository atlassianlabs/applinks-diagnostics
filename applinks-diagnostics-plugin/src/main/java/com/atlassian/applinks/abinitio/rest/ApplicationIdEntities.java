package com.atlassian.applinks.abinitio.rest;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.common.Link;

import com.google.common.collect.Lists;

@XmlRootElement(name = "applicationIds")
public class ApplicationIdEntities extends AbstractEntity {

    @XmlElement(name = "ids")
    private List<ApplicationIdEntity> ids;

    /**
     * Default constructor needed for serialization
     */
    public ApplicationIdEntities() {
    }

    public ApplicationIdEntities(final Iterable<ApplicationIdEntity> ids) {
        this.ids = Lists.newArrayList(ids);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private List<ApplicationIdEntity> ids;
        private List<Link> links = Lists.newArrayList();

        public Builder ids(Iterable<ApplicationIdEntity> ids) {
            this.ids = Lists.newArrayList(ids);
            return this;
        }

        public Builder link(Link link) {
            this.links.add(link);
            return this;
        }

        public ApplicationIdEntities build() {
            ApplicationIdEntities applicationIdEntities = new ApplicationIdEntities(ids);
            applicationIdEntities.setLinks(links);
            return applicationIdEntities;
        }
    }
}
