package com.atlassian.applinks.abinitio;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.core.property.ApplicationLinkProperties;
import com.atlassian.applinks.core.property.PropertyService;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;


/**
 * Implementation of ApplicationLinkConfigurationService directly accessing the underlying propertyservice to remove all the abstraction and avoid being at the mercy of AppLinks and its failure to load stuff.
 */
public class PropertyServiceApplicationLinkConfigurationService implements ApplicationLinkConfigurationService {

    private final PropertyService propertyService;

    public PropertyServiceApplicationLinkConfigurationService(final PropertyService propertyService) {

        this.propertyService = propertyService;
    }

    public Iterable<ApplicationId> getApplicationIds() {
        List<String> list = (List<String>) propertyService.getGlobalAdminProperties().getProperty("application.ids");
        if (list == null)
        {
            list = new ArrayList<String>();
        }
        return new ArrayList<ApplicationId>(Lists.transform(list, new Function<String, ApplicationId>() {
            public ApplicationId apply(final String from) {
                return new ApplicationId(from);
            }
        }));
    }

    @Override
    public Iterable<ApplicationLinkEntry> getApplicationLinks()
    {
        return Iterables.transform(this.getApplicationIds(), new Function<ApplicationId, ApplicationLinkEntry>()
        {
            @Override
            public ApplicationLinkEntry apply(@Nullable ApplicationId applicationId)
            {
                return new ApplicationLinkEntry(applicationId, getApplicationLinkById(applicationId.get()));
            }
        });
    }

    @Override
    public ApplicationLinkProperties getApplicationLinkById(String applicationId) {
        return propertyService.getApplicationLinkProperties(new ApplicationId(applicationId));
    }

    public class ApplicationLinkEntry {
        private final ApplicationId applicationId;
        private final ApplicationLinkProperties applicationLinkProperties;

        public ApplicationLinkEntry(final ApplicationId applicationId, final ApplicationLinkProperties applicationLinkProperties) {

            this.applicationId = applicationId;
            this.applicationLinkProperties = applicationLinkProperties;
        }

        public ApplicationLinkProperties getApplicationLinkProperties()
        {
            return applicationLinkProperties;
        }

        public ApplicationId getApplicationId()
        {
            return applicationId;
        }
    }

}
