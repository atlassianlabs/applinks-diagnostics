package com.atlassian.applinks.abinitio.rest;

import com.atlassian.plugins.rest.common.Link;
import com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "applicationLink")
public class ApplicationLinkEntity extends AbstractEntity {
    @XmlElement(name = "applicationId")
    private String applicationId;

    @XmlElement(name = "rpcUrl")
    private String rpcUrl;

    @XmlElement(name = "displayUrl")
    private String displayUrl;

    @XmlElement(name = "type")
    private String type;

    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "system")
    private Boolean system;

    @XmlElement(name = "primary")
    private Boolean primary;

    @XmlElement(name = "iconUrl")
    private String iconUrl;

    /**
     * Default constructor needed for serialization
     */
    public ApplicationLinkEntity()
    {

    }

    public ApplicationLinkEntity(final String applicationId, final String rpcUrl, final String displayUrl, final String type, final String name, final Boolean primary, final Boolean system, final String iconUrl){

        this.applicationId = applicationId;
        this.rpcUrl = rpcUrl;
        this.displayUrl = displayUrl;
        this.type = type;
        this.name = name;
        this.primary = primary;
        this.system = system;
        this.iconUrl = iconUrl;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String applicationId;
        private String rpcUrl;
        private String displayUrl;
        private String type;
        private String name;
        private Boolean system;
        private Boolean primary;
        private String iconUrl;
        private List<Link> links = Lists.newArrayList();

        public Builder applicationId(String applicationId) {
            this.applicationId = applicationId;
            return this;
        }

        public Builder rpcUrl(String rpcUrl) {
            this.rpcUrl = rpcUrl;
            return this;
        }

        public Builder displayUrl(String displayUrl) {
            this.displayUrl = displayUrl;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder system(Boolean system) {
            this.system = system;
            return this;
        }

        public Builder primary(Boolean primary) {
            this.primary = primary;
            return this;
        }

        public Builder iconUrl(String iconUrl) {
            this.iconUrl = iconUrl;
            return this;
        }

        public Builder link(Link link) {
            this.links.add(link);
            return this;
        }

        public ApplicationLinkEntity build() {
            ApplicationLinkEntity applicationLinkEntity = new ApplicationLinkEntity(this.applicationId, this.rpcUrl, this.displayUrl, this.type, this.name, this.primary, this.system, this.iconUrl);
            applicationLinkEntity.setLinks(this.links);
            return applicationLinkEntity;
        }
    }
}
