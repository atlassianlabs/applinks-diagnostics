package com.atlassian.applinks.Utils;

import java.util.List;
import java.util.Properties;

import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.host.spi.EntityReference;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.sal.api.pluginsettings.PluginSettings;

import com.google.common.collect.Lists;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class EntityLinkUtils
{
    private static final Logger log = LoggerFactory.getLogger(EntityLinkUtils.class);

    // stolen from applinks SalPropertyService
    public static final String ENTITY_PREFIX                   = ApplicationLinkUtils.APPLINKS + "entity";
    public static final String LOCAL_ENTITY_PREFIX             = ApplicationLinkUtils.APPLINKS + "local";
    public static final String LINKED_ENTITIES = "linked.entities";
    private static final String PRIMARY_FMT = "primary.%s";

    public static Properties getPrimaryLink(final TypeAccessor typeAccessor, PluginSettings pluginSettings, EntityReference entityReference)
    {
        String key = EntityLinkUtils.getPrimaryPropertyKey(typeAccessor, entityReference);
        Properties properties = (Properties)pluginSettings.get(key);
        return properties;
    }

    public static List<String> getEncodedLinks(final TypeAccessor typeAccessor, PluginSettings pluginSettings, EntityReference entityReference)
    {
        String key = EntityLinkUtils.getEntityLinksKey(typeAccessor, entityReference);
        Object obj = pluginSettings.get(key);
        if (obj instanceof List)
        {
            return (List<String>)pluginSettings.get(key);
        }
        else
        {
            log.error("Unrecognized value in [%s] [%s]", key, obj == null ? "null" : obj.toString());
            return Lists.newArrayList();
        }
    }

    public static void setEncodedLinks(TypeAccessor typeAccessor, PluginSettings pluginSettings, EntityReference entityReference, List<String> fixedLinks)
    {
        String key = EntityLinkUtils.getEntityLinksKey(typeAccessor, entityReference);
        pluginSettings.remove(key);
        pluginSettings.put(key, fixedLinks);
    }
    public static String getEntityLinksKey(final TypeAccessor typeAccessor, final EntityReference entityReference)
    {
        return EntityLinkUtils.getEntityKeyRoot(typeAccessor, entityReference) + "." +LINKED_ENTITIES;
    }

    public static String getEntityKeyRoot(final TypeAccessor typeAccessor, final EntityReference entityReference)
    {
        return LOCAL_ENTITY_PREFIX + "." + EntityLinkUtils.escape(entityReference.getKey()) + "." + EntityLinkUtils.escape(lookUpTypeId(typeAccessor, entityReference.getType().getClass()).get());
    }

    public static String getPrimaryPropertyKey(final TypeAccessor typeAccessor, final EntityReference entityReference)
    {
        return EntityLinkUtils.getEntityKeyRoot(typeAccessor, entityReference) + "." + String.format(PRIMARY_FMT, lookUpTypeId(typeAccessor, entityReference.getType().getClass()).get());
    }

    public static TypeId lookUpTypeId(final TypeAccessor typeAccessor, final Class<? extends EntityType> localType)
    {
        final EntityType type = typeAccessor.getEntityType(localType);
        if (type == null) {
            throw new IllegalStateException("Couldn't load " + localType.getName() + ", type not installed?");
        }
        return TypeId.getTypeId(type);
    }

    public static String escape(final String s)
    {
        return s.replaceAll("_", "__").replaceAll("\\.", "_");
    }

    public static String getJSONString(final JSONObject obj, final String propertyKey)
    {
        try
        {
            return obj.isNull(propertyKey) ?
                    null :
                    (String) obj.get(propertyKey);
        }
        catch (JSONException je)
        {
            throw new RuntimeException(je);
        }
    }
}
