package com.atlassian.applinks.Utils;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class ApplicationLinkUtils
{
    // stolen from applinks SalPropertyService
    public static final String APPLINKS                        = "applinks.";
    public static final String APPLINKS_GLOBAL_PREFIX          = APPLINKS + "global";
    public static final String APPLICATION_ADMIN_PREFIX        = APPLINKS + "admin";
    public static final String APPLICATION_PREFIX              = APPLINKS + "application";
    public static final String APPLICATION_ID = "applicationId";
}
