package com.atlassian.applinks.diagnostics.rest;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.application.generic.GenericApplicationTypeImpl;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.rest.AbstractResource;
import com.atlassian.applinks.diagnostics.AppLinkDiagnosticService;
import com.atlassian.applinks.diagnostics.AppLinkDiagnosticTask;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.sun.jersey.spi.resource.Singleton;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class defines the REST end point is used to display the list of application links, and also endpoint for triggering the tests (diagnosis)
 *
 * @since ShipIt23
 */
@Path (DiagnoseApplicationLinkResource.DIAGNOSE_APPLICATION_LINK_CONTEXT)
@Consumes ({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Singleton
//@InterceptorChain ({ ContextInterceptor.class, AdminApplicationLinksInterceptor.class })
@ExperimentalApi
public class DiagnoseApplicationLinkResource extends AbstractResource
{
    private static final Logger log = LoggerFactory.getLogger(DiagnoseApplicationLinkResource.class);
    public static final String QUICK_DIAGNOSIS_URL = "rest/applinks/1.0/applicationlink";
    public static final String DIAGNOSE_APPLICATION_LINK_CONTEXT = "diagnoseApplicationLink";

    private final MutatingApplicationLinkService applicationLinkService;
    private final AppLinkDiagnosticService appLinkDiagnosticService;
    private final I18nResolver i18nResolver;

    public DiagnoseApplicationLinkResource
            (
                    final MutatingApplicationLinkService applicationLinkService,
                    final RestUrlBuilder restUrlBuilder,
                    final RequestFactory requestFactory,
                    final InternalTypeAccessor typeAccessor,
                    final AppLinkDiagnosticService appLinkDiagnosticService,
                    final I18nResolver i18nResolver)
    {
        super (restUrlBuilder, typeAccessor, requestFactory, applicationLinkService);
        this.applicationLinkService = applicationLinkService;
        this.appLinkDiagnosticService = appLinkDiagnosticService;
        this.i18nResolver = i18nResolver;
    }

    @GET
    @Path("test/list")
    public Response tests(@QueryParam("scope") final String scope)
    {
        Iterable<AppLinkDiagnosticTask> scopedTasks = Iterables.filter(appLinkDiagnosticService.getAvailableDiagnosticsTasks(), new Predicate<AppLinkDiagnosticTask>() {
            @Override
            public boolean apply(@Nullable AppLinkDiagnosticTask appLinkDiagnosticTask) {
                return StringUtils.isBlank(scope) ? true : scope.equals(appLinkDiagnosticTask.getScope());
            }
        });

        Iterable<TestEntity> tests = Iterables.transform(scopedTasks,
                new Function<AppLinkDiagnosticTask, TestEntity>() {
                    public TestEntity apply(final AppLinkDiagnosticTask task) {
                        Iterable<String> applicationTypes = Iterables.transform(task.getTargetApplicationTypes(), new Function<String, String>() {
                            @Override
                            public String apply(@Nullable String s) {
                                return i18nResolver.getText(s);
                            }
                        });
                        return new TestEntity(i18nResolver.getText(task.getName()), task.getClass().getCanonicalName(), task.getScope(), Joiner.on(",").join(applicationTypes));
                    }
                });

        return Response.ok(tests).build();
    }

    @GET
    @Path("{applinkId}")
    public Response diagnoseApplicationLink(@PathParam("applinkId") final String applicationId)
    {
        try
        {
            final ApplicationLink applicationLink;

            if("all".equals(applicationId))
            {
                applicationLink = null;
            }
            else
            {
                applicationLink = applicationLinkService
                        .getApplicationLink(new ApplicationId(applicationId));

                if(applicationLink == null)
                {
                    return Response.noContent().build();
                }
            }

            final DiagnosticMessages messages = appLinkDiagnosticService.diagnoseApplicationLink(applicationLink);
            return Response.ok(messages).build();
        }
        catch (TypeNotInstalledException e)
        {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("{applinkId}/{testClassName}")
    public Response diagnoseApplicationLink(@PathParam("applinkId") final String applicationId, @PathParam("testClassName") final String testClassName)
    {
        try
        {
            final ApplicationLink applicationLink;

            if("all".equals(applicationId))
            {
                applicationLink = null;
            }
            else
            {
                applicationLink = applicationLinkService
                        .getApplicationLink(new ApplicationId(applicationId));

                if(applicationLink == null)
                {
                    return Response.noContent().build();
                }
            }

            final DiagnosticMessages messages = appLinkDiagnosticService.diagnoseApplicationLink(applicationLink, testClassName);
            return Response.ok(messages).build();
        }
        catch (TypeNotInstalledException e)
        {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("quick/{applinkId}")
    public Response quickDiagnosis(@PathParam("applinkId") final String applicationId)
    {
        try
        {
            final ApplicationLink link = applicationLinkService.getApplicationLink(new ApplicationId(applicationId));
            // TODO refactor this to call a known set of standard tests.
            if(link == null)
            {
                log.info("Invalid applinkId was provided {}", applicationId);
                return Response.noContent().build();
            }

            log.info("Running quick diagnosis for link to {}", link.getName());
            try
            {
                final ApplicationLinkRequestFactory requestFactory = link.createAuthenticatedRequestFactory();
                log.debug("Using {} and auth URI {} for link {}", new Object[]{
                        requestFactory.getClass().getSimpleName(),
                        requestFactory.getAuthorisationURI(),
                        link.getName()});

                String testUrl = "";
                if(!link.getType().getClass().getName().equals(GenericApplicationTypeImpl.class.getName()))
                {
                    testUrl = QUICK_DIAGNOSIS_URL;
                }
                final ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, testUrl);
                request.execute(quickDiagnosisResponseHandler(link));
                return Response.ok().build();
            }
            catch (CredentialsRequiredException e)
            {
                log.info("AppLink not authorized: {}", link.getName());
                return Response.status(Response.Status.UNAUTHORIZED).entity("Credentials required for applink").build();
            }
            catch (ResponseException e)
            {
                log.info("Error from {}: {}", link.getName(), e.getMessage());
                return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Error from linked application: " + e.getMessage()).build();
            }
        }
        catch (TypeNotInstalledException e)
        {
            return Response.noContent().build();
        }
    }

    private static ApplicationLinkResponseHandler<String> quickDiagnosisResponseHandler(final ApplicationLink link)
    {
        return new ApplicationLinkResponseHandler<String>()
        {
            public String handle(final com.atlassian.sal.api.net.Response response) throws ResponseException
            {
                log.debug("Successful {} response from {}", response.getStatusCode(), link.getName());
                if (response.getStatusCode() != 200)
                {
                    throw new ResponseException(response.getStatusText());
                }
                else
                {
                    return response.getResponseBodyAsString();
                }
            }

            public String credentialsRequired(final com.atlassian.sal.api.net.Response response) throws ResponseException
            {
                log.debug("Credential required {} response from {}", response.getStatusCode(), link.getName());
                throw new ResponseException(response.getStatusText());
            }
        };
    }
}
