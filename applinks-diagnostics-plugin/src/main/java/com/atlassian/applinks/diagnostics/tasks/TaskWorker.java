package com.atlassian.applinks.diagnostics.tasks;

/**
 * The task worker interface for running the diagnosis/tests
 *
 * @since v6.0
 */
public interface TaskWorker
{
    boolean runDiagnosis();
}
