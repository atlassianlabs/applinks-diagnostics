package com.atlassian.applinks.diagnostics.rest;

import com.atlassian.applinks.core.rest.model.LinkedEntity;
import com.atlassian.plugins.rest.common.Link;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents the Configuration an AuthenticationProvider for an ApplicationLink
 *
 * @since v3.12
 */
@XmlRootElement (name = "authenticationProvider")
public class AuthenticationProviderEntity extends LinkedEntity
{
    @XmlElement (name = "config")
    private HashMap<String, String> config;
    @XmlElement (name = "module")
    private String module;
    @XmlElement (name = "provider")
    private String provider;

    public AuthenticationProviderEntity()
    {

    }

    public AuthenticationProviderEntity(final Link self, final String module, final String provider, final Map<String, String> config)
    {
        this.module = module;
        this.provider = provider;
        if(config != null)
        {
            this.config = new HashMap<String, String>(config);
        }

        this.addLink(self);
    }

    @Nullable
    public HashMap<String, String> getConfig()
    {
        return config;
    }

    @Nullable
    public String getModule()
    {
        return module;
    }

    @Nullable
    public String getProvider()
    {
        return provider;
    }
}
