package com.atlassian.applinks.diagnostics;

import java.util.Map;

import com.atlassian.applinks.api.ApplicationLink;

/**
 * The interface for all diagnose tasks
 */
public interface AppLinkDiagnosticTask extends DiagnosticTask
{
    boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages);

    boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages, Map<String, String> testParameters);
}
