package com.atlassian.applinks.diagnostics;

import javax.xml.bind.annotation.XmlElement;

import com.atlassian.sal.api.net.Request;

import static com.atlassian.applinks.diagnostics.TaskAction.ActionType.AUTHENTICATE;
import static com.atlassian.applinks.diagnostics.TaskAction.ActionType.FIX;
import static com.atlassian.applinks.diagnostics.TaskAction.ActionType.KNOWLEDGE_BASE;
import static com.atlassian.applinks.diagnostics.TaskAction.ActionType.INSTALL;
import static com.atlassian.sal.api.net.Request.MethodType.GET;
import static com.atlassian.sal.api.net.Request.MethodType.POST;

/**
 * @since ShipIt23
 */
public class TaskAction
{
    public static TaskAction authenticate(String id, String appName, String appUri, String authUri, String content)
    {
        return new AuthAction(AUTHENTICATE, GET, id, appName, appUri, authUri, content);
    }

    public static TaskAction fix(String url, final String description)
    {
        return new UrlAction(url, FIX, POST, description);
    }

    public static TaskAction knowledgeBase(String url, final String description)
    {
        return new UrlAction(url, KNOWLEDGE_BASE, POST, description);
    }

    public static TaskAction install()
    {
        return new TaskAction(INSTALL, GET);
    }

    private final ActionType type;
    private final Request.MethodType method;

    public static enum ActionType
    {
        AUTHENTICATE, FIX, KNOWLEDGE_BASE, INSTALL;
    }

    /**
     * required for xml serialization
     */
    private TaskAction()
    {
        type = null;
        method = null;
    }

    private TaskAction(ActionType type, Request.MethodType method)
    {
        this.type = type;
        this.method = method;
    }

    @XmlElement
    public ActionType getActionType()
    {
        return type;
    }

    @XmlElement
    public Request.MethodType getMethod()
    {
        return method;
    }

    public static class UrlAction extends TaskAction
    {
        private final String actionUrl;
        private final String description;

        private UrlAction(String url, ActionType actionType, Request.MethodType methodType, String description)
        {
            super(actionType, methodType);
            this.actionUrl = url;
            this.description = description;
        }

        @XmlElement
        public String getActionUrl()
        {
            return actionUrl;
        }

        @XmlElement
        public String getDescription()
        {
            return description;
        }
    }

    public static class AuthAction extends TaskAction
    {
        private final String id;
        private final String appName;
        private final String appUri;
        private final String authUri;
        private final String content;

        private AuthAction(ActionType actionType, Request.MethodType methodType, String id, String appName, String appUri, String authUri, String content)
        {
            super(actionType, methodType);
            this.id = id;
            this.appName = appName;
            this.appUri = appUri;
            this.authUri = authUri;
            this.content = content;
        }

        @XmlElement
        public String getId()
        {
            return id;
        }

        @XmlElement
        public String getAppName()
        {
            return appName;
        }

        @XmlElement
        public String getAppUri()
        {
            return appUri;
        }

        @XmlElement
        public String getAuthUri()
        {
            return authUri;
        }

        @XmlElement
        public String getContent()
        {
            return content;
        }
    }
}
