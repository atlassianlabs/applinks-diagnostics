package com.atlassian.applinks.diagnostics.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.servlet.AbstractSecureServlet;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;

import com.google.common.collect.Maps;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class AuthenticationServlet extends AbstractSecureServlet
{
    public AuthenticationServlet(final UserManager userManager,
            final LoginUriProvider loginUriProvider) {
        super(userManager, loginUriProvider);
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        super.doGet(request, response);
    }
}
