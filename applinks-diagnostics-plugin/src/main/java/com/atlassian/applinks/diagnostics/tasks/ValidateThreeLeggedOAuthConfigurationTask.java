package com.atlassian.applinks.diagnostics.tasks;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.application.bamboo.BambooApplicationTypeImpl;
import com.atlassian.applinks.application.confluence.ConfluenceApplicationTypeImpl;
import com.atlassian.applinks.application.fecru.FishEyeCrucibleApplicationTypeImpl;
import com.atlassian.applinks.application.jira.JiraApplicationTypeImpl;
import com.atlassian.applinks.application.refapp.RefAppApplicationTypeImpl;
import com.atlassian.applinks.application.stash.StashApplicationTypeImpl;
import com.atlassian.applinks.core.auth.oauth.OAuthAuthenticatorProviderPluginModule;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.applinks.diagnostics.rest.ApplicationLinkConfigurationService;
import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

/**
 * Currently disabled
 *
 * @since ShipIt23
 */
public class ValidateThreeLeggedOAuthConfigurationTask extends AbstractApplinkDiagnosticTask
{
    private final ApplicationLinkConfigurationService applicationLinkConfigurationService;
    private final HostApplication hostApplication;

    // TODO i18n
    public static final String TEST_NAME = "3LO Configuration";
    public static final String URL = "5IjQIw";

    public ValidateThreeLeggedOAuthConfigurationTask(ApplicationLinkConfigurationService applicationLinkConfigurationService,
                                                     HostApplication hostApplication)
    {
        this.applicationLinkConfigurationService = applicationLinkConfigurationService;
        this.hostApplication = hostApplication;
    }

    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages)
    {
        final TaskResult taskResult = new TaskResult(this.getName());

        if(!this.diagnose(taskResult, applicationLink))
        {
            diagnosticMessages.addTaskResults(taskResult);
            return false;
        }

        taskResult.details(String.format("Checking that the 3-Leg OAuth configuration is consistent between here and '%s'", applicationLink.getName()));

        List<AuthenticationProviderPluginModule> configureModules =  applicationLinkConfigurationService.getConfiguredProviderPluginModules(applicationLink, new Predicate<AuthenticationProviderPluginModule>() {
            public boolean apply(@Nullable AuthenticationProviderPluginModule authenticationProviderPluginModule) {
                return (authenticationProviderPluginModule instanceof OAuthAuthenticatorProviderPluginModule);
            }
        });

        if(configureModules.size() == 0)
        {
            taskResult.fail("Oh we can't find the local 3-Leg OAuth configuration.");
            return true;
        }

        if(this.applicationLinkConfigurationService.isConfigured(applicationLink, configureModules.get(0)))
        {
            String response = this.performAuthenticatedRequest(applicationLink, String.format("%s/rest/applinks/2.0/applicationlink/%s/authentication/consumer", applicationLink.getRpcUrl() , hostApplication.getId()), taskResult);

            //TODO do some checking
            if (response != null)
            {
                taskResult.pass("Nothing to complain about here!");
            }
        }

        diagnosticMessages.addTaskResults(taskResult);
        return false;
    }

    public ArrayList<String> getTargetApplicationTypes() {
        // I know this is shit but...
        return Lists.newArrayList(
                new BambooApplicationTypeImpl(null, null).getI18nKey(),
                new ConfluenceApplicationTypeImpl(null, null).getI18nKey(),
                new FishEyeCrucibleApplicationTypeImpl(null, null).getI18nKey(),
                new JiraApplicationTypeImpl(null, null).getI18nKey(),
                new RefAppApplicationTypeImpl(null, null).getI18nKey(),
                new StashApplicationTypeImpl(null, null).getI18nKey()
        );
    }

    public String getName()
    {
        return TEST_NAME;
    }

    public String getURL() {
        return URL;
    }
}
