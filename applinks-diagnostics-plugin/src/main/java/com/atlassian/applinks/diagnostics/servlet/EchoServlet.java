package com.atlassian.applinks.diagnostics.servlet;

import com.google.common.collect.Maps;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;

import com.atlassian.applinks.servlet.AbstractSecureServlet;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

public class EchoServlet extends AbstractSecureServlet
{

    public static final String REQUEST_URL = "requestURL";
    public static final String REQUEST = "request";

    public EchoServlet(final UserManager userManager,
                       final LoginUriProvider loginUriProvider) {
        super(userManager, loginUriProvider);
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        // TODO: check authorization - this means the test will need to gain authorisation when accessing this URL
        //super.doGet(request, response);

        JSONObject jsonObject = new JSONObject();
        Map<String, Object> requestAttributes = Maps.newTreeMap();

        requestAttributes.put("method", request.getMethod());
        requestAttributes.put("scheme", request.getScheme());
        requestAttributes.put("serverName", request.getServerName());
        requestAttributes.put("serverPort", request.getServerPort());
        requestAttributes.put("contextPath", request.getContextPath());
        requestAttributes.put("queryString", request.getQueryString());
        requestAttributes.put("pathInfo", request.getPathInfo());
        requestAttributes.put("pathTranslated", request.getPathTranslated());
        requestAttributes.put("remoteUser", request.getRemoteUser());
        requestAttributes.put("requestURI", request.getRequestURI());
        requestAttributes.put(REQUEST_URL, request.getRequestURL().toString());
        requestAttributes.put("requestedSessionId", request.getRequestedSessionId());
        requestAttributes.put("protocol", request.getProtocol());
        requestAttributes.put("remoteAddr", request.getRemoteAddr());
        requestAttributes.put("remoteHost", request.getRemoteHost());


        // headers
        Map<String, Object> headers = Maps.newHashMap();
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            headers.put(headerName, request.getHeader(headerName));
        }
        requestAttributes.put("headers", headers);

        // parameters
        Map<String, Object> parameters = Maps.newHashMap();
        Enumeration parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String parameterName = (String) parameterNames.nextElement();
            parameters.put(parameterName, request.getHeader(parameterName));
        }
        requestAttributes.put("parameters", parameters);

        // output
        PrintWriter out = response.getWriter();
        response.addHeader("Content-Type", "application/json");
        try {
            jsonObject.put(REQUEST, requestAttributes);
        } catch (JSONException e) {

        }
        out.println(jsonObject.toString());
    }

}
