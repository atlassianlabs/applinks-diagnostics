package com.atlassian.applinks.diagnostics;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.applinks.diagnostics.StatusMessage.Status.ERROR;
import static com.atlassian.applinks.diagnostics.StatusMessage.Status.FAIL;
import static com.atlassian.applinks.diagnostics.StatusMessage.Status.INFO;
import static com.atlassian.applinks.diagnostics.StatusMessage.Status.PASS;
import static com.atlassian.applinks.diagnostics.StatusMessage.Status.WARNING;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.unmodifiableList;

/**
 * Return the result of a task/diagnosis
 */
public class TaskResult {

    private static final Logger log = LoggerFactory.getLogger(TaskResult.class);

    private final String name;
    private String details;
    private final List<TaskAction> actions = newArrayList();
    private final List<StatusMessage> statusMessages = newArrayList();

    /**
     * required for xml serialization
     */
    private TaskResult()
    {
        this.name = null;
    }

    public TaskResult(final String name)
    {
        this.name = name;
    }

    public TaskResult details(String details)
    {
        this.details = details;
        return this;
    }

    public TaskResult addAction(TaskAction taskAction)
    {
        actions.add(taskAction);
        return this;
    }

    public TaskResult error(Exception ex)
    {
        return error(ex.toString() + ":" + ex.getMessage());
    }

    public TaskResult error(String title, Exception ex)
    {
        return error(title, ex.getMessage());
    }

    public TaskResult fail(String message)
    {
        return fail(null, message);
    }

    public TaskResult fail(String title, String message)
    {
        return addMessage(FAIL, title, message);
    }

    public TaskResult pass(String message)
    {
        return pass(null, message);
    }

    public TaskResult pass(String title, String message)
    {
        return addMessage(PASS, title, message);
    }

    public TaskResult info(String message)
    {
        return info(null, message);
    }

    public TaskResult info(String title, String message)
    {
        return addMessage(INFO, title, message);
    }

    public TaskResult warning(String message)
    {
        return warning(null, message);
    }

    public TaskResult warning(String title, String message)
    {
        return addMessage(WARNING, title, message);
    }

    public TaskResult error(String message)
    {
        return error(null, message);
    }

    public TaskResult error(String title, String message)
    {
        return addMessage(ERROR, title, message);
    }

    private TaskResult addMessage(StatusMessage.Status severity, String title, String message)
    {
        log.debug(message);
        statusMessages.add(new StatusMessage(severity, title, message));
        return this;
    }

    @SuppressWarnings("UnusedDeclaration")
    @XmlElement
    public synchronized List<StatusMessage> getStatusMessages()
    {
        return unmodifiableList(statusMessages);
    }

    @XmlElement
    public synchronized String getName()
    {
        return name;
    }

    @XmlElement
    public synchronized String getDetails()
    {
        return details;
    }

    @XmlElement
    public synchronized List<TaskAction> getActions()
    {
        return actions;
    }
}
