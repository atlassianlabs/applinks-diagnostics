package com.atlassian.applinks.diagnostics.tasks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.diagnostics.TaskAction;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.applinks.diagnostics.servlet.EchoServlet;
import com.google.common.collect.Maps;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.ObjectUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Set;

import static com.atlassian.applinks.diagnostics.KnowledgeBaseArticles.TOO_MANY_REDIRECTS;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.String.format;

/**
 * The task worker for diagnosing HTTP echo to the remote application to verify if the applink diagnostics plugin is installed on the target instance
 */
public class HttpEchoTaskWorker {
    private static final String DIAGNOSTICS_ECHO_PATH = "plugins/servlet/applinksDiagnostics/echo";

    private static final Set<Integer> REDIRECT_CODES = newHashSet(301, 302, 303, 307);
    private static final Set<Integer> SUCCESS_CODES = newHashSet(200, 401);

    private static final String RPC_PATH = "rest/applinks/1.0/applicationlink";
    private static final int MAX_REDIRECTS = 3; // Should be the same as in HttpClientRequest in SAL-Core

    private final TaskResult taskResult;
    private final HttpClient httpClient;
    private ApplicationLink applicationLink;
    private final Map<String, String> testParameters;

    public HttpEchoTaskWorker(ApplicationLink applicationLink, TaskResult taskResult) {
        this.taskResult = taskResult;
        this.applicationLink = applicationLink;
        this.testParameters = Maps.newHashMap();
        httpClient = new HttpClient();
    }

    public boolean runDiagnosis() {
        String url = this.buildUrl(applicationLink.getRpcUrl().toASCIIString(), DIAGNOSTICS_ECHO_PATH);
        taskResult.details(format("Testing HTTP echo to %s", applicationLink.getName()));
        GetMethod request = createRequest(url);


        int redirectCount = 0;
        do {
            try {
                final int statusCode = httpClient.executeMethod(request);
                final Header[] responseHeaders = request.getResponseHeaders();
                final String responseBody = request.getResponseBodyAsString();
                final Header[] responseFooters = request.getResponseFooters();

                Map<String, String> headers = Maps.newHashMapWithExpectedSize(responseHeaders.length + responseFooters.length);
                for (Header h : responseHeaders) {
                    headers.put(h.getName(), h.getValue());
                }

                for (Header h : responseFooters) {
                    headers.put(h.getName(), h.getValue());
                }

                DefaultResponseHandler.recordResponse(statusCode, headers, responseBody, taskResult);

                if (REDIRECT_CODES.contains(statusCode)) {
                    final Header header = request.getResponseHeader("location");
                    if (header == null) {
                        return noRedirectLocation(request, statusCode);
                    } else {
                        final String redirectLocation = header.getValue();
                        receivedRedirect(request, statusCode, redirectLocation);
                        request = createRequest(redirectLocation);
                        redirectCount++;
                    }
                } else if (SUCCESS_CODES.contains(statusCode)) {
                    return verifyEcho(request, taskResult);
                } else {
                    return responseCodeUnexpected(request, statusCode);
                }
            } catch (IOException e) {
                taskResult.error(e);
                return false;
            } catch (URISyntaxException e) {
                taskResult.error(e);
                return false;
            } finally {
                request.releaseConnection();
            }
        }
        while (redirectCount < MAX_REDIRECTS);

        taskResult.warning("Redirect count from " + url + " exceeded maximum of " + MAX_REDIRECTS);
        taskResult.addAction(TOO_MANY_REDIRECTS.toAction());
        return false;
    }

    private boolean verifyEcho(GetMethod request, TaskResult taskResult) {
        String responseBody = null;
        Boolean success = true;
        try {
            responseBody = request.getResponseBodyAsString();
            taskResult.info(String.format("Response [%s]", responseBody));
            JSONObject responseObject = new JSONObject(responseBody);
            JSONObject requestObject = responseObject.getJSONObject(EchoServlet.REQUEST);
            String echoedRequestURL = requestObject.getString(EchoServlet.REQUEST_URL);
            if (!request.getURI().toString().equals(echoedRequestURL)) {
                taskResult.error(String.format("Request URL is inconsistent. Sent [%s] Received [%s]", request.getURI().toString(), echoedRequestURL));
                success = false;
            }
        } catch (IOException e) {
            taskResult.error("Unable to get response body");
            success = false;
        } catch (JSONException e) {
            taskResult.error(String.format("Unable to parse response [%s] : [%s]", responseBody, e.getMessage()));
            success = false;
        }

        if(!success)
        {
            taskResult.fail("Echo response is inconsistent with request");
        }
        else
        {
            taskResult.pass("Echo response is consistent");
        }
        return success;
    }

    private static String buildUrl(String url, String relative) {
        if (!url.endsWith("/")) {
            return url + '/' + relative;
        } else {
            return url + relative;
        }
    }

    private GetMethod createRequest(String url) {
        final GetMethod get = new GetMethod(url);
        get.setFollowRedirects(false);

        taskResult.info(format("Attempting to ping %s", url));

        return get;
    }

    private boolean responseCodeUnexpected(GetMethod request, int statusCode) throws URIException {
        // If it's a 404, plugin is likely not installed.
        if (statusCode == 404) {
            taskResult.warning("The AppLinks Diagnostic Plugin does not appear to be installed in "
                    + applicationLink.getName());
            taskResult.addAction(TaskAction.install());
        } else {
            taskResult.warning("Received unexpected HTTP " + statusCode + " response from " + request
                    .getURI() + " using " + this);
        }
        return false;
    }

    private void receivedRedirect(GetMethod request, int statusCode, String redirectLocation) throws URIException, URISyntaxException {
        taskResult.info("Received HTTP " + statusCode + " redirect " + redirectDescription(
                new URI(request.getURI().toString()), new URI(redirectLocation)));
    }

    private static String redirectDescription(URI requestUrl, URI redirectUrl) {
        if (!ObjectUtils.equals(requestUrl.getScheme(), redirectUrl.getScheme())) {
            return "to " + redirectUrl.getScheme();
        } else if (!ObjectUtils.equals(requestUrl.getPath(), redirectUrl.getPath())) {
            return "to " + redirectUrl.getPath();
        } else {
            return "to " + redirectUrl;
        }
    }

    private boolean noRedirectLocation(GetMethod request, int statusCode) throws URIException {
        taskResult.error("Got HTTP " + statusCode + " redirect from " + request
                .getURI() + " but no redirect location was specified");
        return false;
    }
}
