package com.atlassian.applinks.diagnostics.tasks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.application.bamboo.BambooApplicationTypeImpl;
import com.atlassian.applinks.application.confluence.ConfluenceApplicationTypeImpl;
import com.atlassian.applinks.application.fecru.FishEyeCrucibleApplicationTypeImpl;
import com.atlassian.applinks.application.jira.JiraApplicationTypeImpl;
import com.atlassian.applinks.application.refapp.RefAppApplicationTypeImpl;
import com.atlassian.applinks.application.stash.StashApplicationTypeImpl;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.google.common.collect.Lists;

import java.util.ArrayList;

/**
 * Performs HTTP echo diagnosis to see if the remote application has applink diagnostics plugin installed (also validate the plugin version)
 */
public class HttpEchoDiagnosticTask extends AbstractApplinkDiagnosticTask {
    public static final String TEST_NAME = "HTTP Echo";
    public static final String URL = "24jQIw";

    @Override
    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages) {
        final TaskResult taskResult = new TaskResult(TEST_NAME);
        this.addKnowledgeBaseLink(taskResult);

        if (applicationLink == null)
        {
            taskResult.fail("Application Link is NULL");
            return false;
        }
        diagnosticMessages.addTaskResults(taskResult);

        if (applicationLink.getRpcUrl() == null)
        {
            taskResult.fail("Application URL is NULL");
            return false;
        }
        final HttpEchoTaskWorker taskWorker = new HttpEchoTaskWorker(applicationLink, taskResult);
        return taskWorker.runDiagnosis();
    }

    @Override
    public ArrayList<String> getTargetApplicationTypes() {
        // I know this is shit but...
        return Lists.newArrayList(
                new BambooApplicationTypeImpl(null, null).getI18nKey(),
                new ConfluenceApplicationTypeImpl(null, null).getI18nKey(),
                new FishEyeCrucibleApplicationTypeImpl(null, null).getI18nKey(),
                new JiraApplicationTypeImpl(null, null).getI18nKey(),
                new RefAppApplicationTypeImpl(null, null).getI18nKey(),
                new StashApplicationTypeImpl(null, null).getI18nKey());
    }

    @Override
    public String getName() {
        return TEST_NAME;
    }

    public String getURL() {
        return URL;
    }
}
