package com.atlassian.applinks.diagnostics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.core.property.PropertyService;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.util.concurrent.NotNull;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The service that collects all the enabled diagnostics tasks (via ApplinkDiagnosticTaskModuleDescriptor.class) and run the diagnoses
 */
public class AppLinkDiagnosticService
{
    private static final Logger LOG = LoggerFactory.getLogger(AppLinkDiagnosticService.class);

    private final PluginAccessor pluginAccessor;
    private final PropertyService propertyService;
    private Iterable<ApplicationLink> applicationLinks;

    public AppLinkDiagnosticService(final PluginAccessor pluginAccessor, final PropertyService propertyService)
    {
        this.pluginAccessor = pluginAccessor;
        this.propertyService = propertyService;
    }

    public DiagnosticMessages diagnoseApplicationLink(final ApplicationLink appLink)
    {
        final DiagnosticMessages messages = new DiagnosticMessages();
        try
        {
            for (AppLinkDiagnosticTask task : Iterables.filter(getAvailableDiagnosticsTasks(), getFilterByApplicationTypePredicate(appLink)))
            {
                final boolean mayContinue = task.diagnose(appLink, messages);

                // for now continue tests even after a failure
//                if (!mayContinue)
//                {
//                    break;
//                }
            }
        }
        catch (RuntimeException e)
        {
            LOG.error("Diagnostics failed", e);
            messages.error(e);
        }
        return messages;
    }

    public DiagnosticMessages diagnoseApplicationLink(final ApplicationLink appLink, final String testClassName)
    {
        return diagnoseApplicationLink(appLink, testClassName, Maps.<String, String>newHashMap());
    }

    public DiagnosticMessages diagnoseApplicationLink(final ApplicationLink appLink, final String testClassName, final Map<String,String> testParameters)
    {
        final DiagnosticMessages messages = new DiagnosticMessages();
        try
        {
            for (AppLinkDiagnosticTask task : Iterables.filter(Iterables.filter(getAvailableDiagnosticsTasks(), getFilterByApplicationTypePredicate(appLink)), getFilterByTestClassNamePredicate(testClassName)))
            {
                final boolean mayContinue = task.diagnose(appLink, messages);

                // for now continue tests even after a failure
//                if (!mayContinue)
//                {
//                    break;
//                }
            }
        }
        catch (RuntimeException e)
        {
            LOG.error("Diagnostics failed", e);
            messages.getTaskResults().get(0).error("Diagnostics failed: " + e.toString() + ":" + e.getMessage());
        }
        return messages;
    }

    private Predicate<AppLinkDiagnosticTask> getFilterByApplicationTypePredicate(final ApplicationLink appLink) {
        return new Predicate<AppLinkDiagnosticTask>() {
            public boolean apply(@NotNull AppLinkDiagnosticTask appLinkDiagnosticTask) {

                for(String typeId : appLinkDiagnosticTask.getTargetApplicationTypes())
                {
                    if(appLink == null)
                    {
                        // no specifi applink so run for all.
                        return true;
                    }

                    // ffs I give up trying to do this properly  for now.
                    if(appLink.getType().getI18nKey().equals(typeId))
                    {
                        return true;
                    }
                }
                return false;
            }
        };
    }

    private Predicate<AppLinkDiagnosticTask> getFilterByTestClassNamePredicate(final String testClassName) {
        return new Predicate<AppLinkDiagnosticTask>() {
            public boolean apply(@NotNull AppLinkDiagnosticTask appLinkDiagnosticTask)
            {
                return appLinkDiagnosticTask.getClass().getCanonicalName().equals(testClassName);
            }
        };
    }

    /**
     * Get the all defined diagnostics tasks.
     * @return
     */
    public Iterable<AppLinkDiagnosticTask> getAvailableDiagnosticsTasks() {

        return Iterables.transform(pluginAccessor.getEnabledModuleDescriptorsByClass(ApplinkDiagnosticTaskModuleDescriptor.class),
                new Function<ApplinkDiagnosticTaskModuleDescriptor, AppLinkDiagnosticTask>() {
                    public AppLinkDiagnosticTask apply(final ApplinkDiagnosticTaskModuleDescriptor from) {
                        LOG.error(String.format("Found Task '%s'", from.getModule().getName()));
                        return from.getModule();
                    }
                });
    }

    public Iterable<ApplicationId> getApplicationIds() {
        List<String> list = (List<String>) propertyService.getGlobalAdminProperties().getProperty("application.ids");
        if (list == null)
        {
            list = new ArrayList<String>();
        }
        return new ArrayList<ApplicationId>(Lists.transform(list, new Function<String, ApplicationId>() {
            public ApplicationId apply(final String from) {
                return new ApplicationId(from);
            }
        }));
    }
}
