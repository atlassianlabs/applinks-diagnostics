package com.atlassian.applinks.diagnostics;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class StatusMessage
{
    public static enum Status
    {
        INFO, WARNING, ERROR, FAIL, PASS;
    }

    private final Status status;
    private final String text;
    private final String title;

    /**
     * required for xml serialization
     */
    public StatusMessage()
    {
        this.status = null;
        this.title = null;
        this.text = null;
    }

    public StatusMessage(Status status, String title, String text)
    {
        this.status = status;
        this.title = title;
        this.text = text;
    }

    @XmlAttribute
    public Status getStatus()
    {
        return status;
    }

    @XmlElement
    public String getTitle()
    {
        return title;
    }

    @XmlElement
    public String getText()
    {
        return text;
    }
}
