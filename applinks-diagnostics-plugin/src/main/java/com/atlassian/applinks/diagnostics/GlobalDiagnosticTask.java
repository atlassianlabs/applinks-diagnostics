package com.atlassian.applinks.diagnostics;

import java.util.Map;

public interface GlobalDiagnosticTask extends DiagnosticTask
{
    boolean diagnose(DiagnosticMessages diagnosticMessages);

    boolean diagnose(DiagnosticMessages diagnosticMessages, Map<String, String> testParameters);
}
