package com.atlassian.applinks.diagnostics;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.unmodifiableList;

@XmlRootElement
public class DiagnosticMessages
{
    private final List<TaskResult> results = newArrayList();
    private final List<StatusMessage> statusMessages = newArrayList();

    @SuppressWarnings("UnusedDeclaration")
    @XmlElement
    public List<TaskResult> getTaskResults()
    {
        return unmodifiableList(results);
    }

    public void addTaskResults(TaskResult taskResult) {
        results.add(taskResult);
    }

    public DiagnosticMessages error(Exception ex)
    {
        if(StringUtils.isBlank(ex.getMessage()))
        {
            return error(ex.toString());
        }
        else
        {
            return error(ex.getMessage());
        }
    }

    private DiagnosticMessages error(String message)
    {
        return addMessage(message, StatusMessage.Status.ERROR);
    }

    private DiagnosticMessages addMessage(String message, StatusMessage.Status severity)
    {
        statusMessages.add(new StatusMessage(severity, null, message));
        return this;
    }

}
