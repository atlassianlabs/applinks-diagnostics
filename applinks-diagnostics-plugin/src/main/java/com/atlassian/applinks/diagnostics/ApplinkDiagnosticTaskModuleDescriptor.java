package com.atlassian.applinks.diagnostics;

import org.apache.commons.lang.StringUtils;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.dom4j.Element;

/**
 * This is the module descriptor for all diagnostics tasks defined in the plugin
 * The tasks are defined in the plugin descriptor (atlassian-plugin.xml) as diagnosis-task
 *
 * @since ShipIt23
 */
public class ApplinkDiagnosticTaskModuleDescriptor extends AbstractModuleDescriptor<AppLinkDiagnosticTask> {
    private String applicationType;
    private String scope;

    public ApplinkDiagnosticTaskModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException {
        super.init(plugin, element);
        applicationType = element.attributeValue("applicationType");
        scope = element.attributeValue("scope");
        if(StringUtils.isBlank(scope)){
            scope = "applicationLink";
        }

    }

    public AppLinkDiagnosticTask getModule() {
        AppLinkDiagnosticTask appLinkDiagnosticTask = moduleFactory.createModule(moduleClassName, this);
        appLinkDiagnosticTask.setScope(this.scope);
        return appLinkDiagnosticTask;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public String getScope() {
        return this.scope;
    }
}
