package com.atlassian.applinks.diagnostics.tasks;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Set;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.sal.core.net.HttpClientRequest;

import com.google.common.collect.Maps;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.ObjectUtils;

import static com.atlassian.applinks.diagnostics.KnowledgeBaseArticles.TOO_MANY_REDIRECTS;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.String.format;

/**
 * The task worker for diagnosing HTTP connectivity (includes retrieving application manifest, checking the RPC URL) between 2 instances
 */
public class HttpConnectivityTaskWorker
{
    private static final Set<Integer> REDIRECT_CODES = newHashSet(301, 302, 303, 307);
    private static final Set<Integer> SUCCESS_CODES = newHashSet(200, 401);

    private static final String RPC_PATH = "rest/applinks/1.0/applicationlink";
    private static final int MAX_REDIRECTS = HttpClientRequest.MAX_REDIRECTS; // Should be the same as in HttpClientRequest in SAL-Core

    private final TaskResult taskResult;
    private final HttpClient httpClient;
    private ApplicationLink applicationLink;
    private final URI testUrl;

    public HttpConnectivityTaskWorker(ApplicationLink applicationLink, TaskResult taskResult, URI testUrl)
    {
        this.taskResult = taskResult;
        this.applicationLink = applicationLink;
        this.testUrl = testUrl;
        httpClient = new HttpClient();
    }

    public boolean runDiagnosis()
    {
        taskResult.details(format("Testing HTTP connectivity to [%s] for [%s]", this.testUrl.toASCIIString(), applicationLink.getName()));
        return testPingUrl(this.testUrl.toASCIIString());
    }

    private boolean testPingUrl(String url) {
        GetMethod request = createRequest(url);
        int redirectCount = 0;
        do
        {
            try
            {
                final int statusCode = httpClient.executeMethod(request);
                final Header[] responseHeaders = request.getResponseHeaders();
                final String responseBody = request.getResponseBodyAsString();
                final Header[] responseFooters = request.getResponseFooters();

                Map<String, String> headers = Maps.newHashMapWithExpectedSize(responseHeaders.length + responseFooters.length);
                for(Header h : responseHeaders)
                {
                    headers.put(h.getName(), h.getValue());
                }

                for(Header h : responseFooters)
                {
                    headers.put(h.getName(), h.getValue());
                }

                DefaultResponseHandler.recordResponse(statusCode, headers, responseBody, taskResult);

                if (REDIRECT_CODES.contains(statusCode))
                {
                    final Header header = request.getResponseHeader("location");
                    if (header == null)
                    {
                        return noRedirectLocation(request, statusCode);
                    }
                    else
                    {
                        final String redirectLocation = header.getValue();
                        receivedRedirect(request, statusCode, redirectLocation);
                        request = createRequest(redirectLocation);
                        redirectCount++;
                    }
                }
                else if (SUCCESS_CODES.contains(statusCode))
                {
                    return successfullyPinged();
                }
                else
                {
                    return responseCodeUnexpected(request, statusCode);
                }
            }
            catch (UnknownHostException e)
            {
                taskResult.error(format("Unknown Host [%s]", url));
                return false;
            }
            catch (IOException e)
            {
                taskResult.error(e);
                return false;
            }
            catch (URISyntaxException e)
            {
                taskResult.error(format("Invalid URI [%s]", url));
                return false;
            }
            finally
            {
                request.releaseConnection();
            }
        }
        while (redirectCount < MAX_REDIRECTS);

        taskResult.warning("Redirect count from " + url + " exceeded maximum of " + MAX_REDIRECTS);
        taskResult.addAction(TOO_MANY_REDIRECTS.toAction());
        return false;
    }

    private boolean failedToEstablishConnection()
    {
        taskResult.fail("Failed to ping " + applicationLink.getName());
        return false;
    }

    private static String buildDefaultRestUrl(String url)
    {
        return buildUrl(url, RPC_PATH);
    }

    private static String buildUrl(String url, String relative)
    {
        if (!url.endsWith("/"))
        {
            return url + '/' + relative;
        }
        else
        {
            return url + relative;
        }
    }

    private GetMethod createRequest(String url)
    {
        final GetMethod get = new GetMethod(url);
        get.setFollowRedirects(false);

        taskResult.info(format("Attempting to ping %s", url));

        return get;
    }

    private boolean successfullyPinged() throws URIException
    {
        taskResult.pass(format("Successfully pinged to %s", applicationLink.getName()));
        return true;
    }

    private boolean responseCodeUnexpected(GetMethod request, int statusCode) throws URIException
    {
        taskResult.warning("Received unexpected HTTP " + statusCode + " response from " + request
                .getURI() + " using " + this);
        return false;
    }

    private void receivedRedirect(GetMethod request, int statusCode, String redirectLocation) throws URIException, URISyntaxException
    {
        taskResult.info("Received HTTP " + statusCode + " redirect " + redirectDescription(
                new URI(request.getURI().toString()), new URI(redirectLocation)));
    }

    private static String redirectDescription(URI requestUrl, URI redirectUrl)
    {
        if (!ObjectUtils.equals(requestUrl.getScheme(), redirectUrl.getScheme()))
        {
            return "to " + redirectUrl.getScheme();
        }
        else if (!ObjectUtils.equals(requestUrl.getPath(), redirectUrl.getPath()))
        {
            return "to " + redirectUrl.getPath();
        }
        else
        {
            return "to " + redirectUrl;
        }
    }

    private boolean noRedirectLocation(GetMethod request, int statusCode) throws URIException
    {
        taskResult.error("Got HTTP " + statusCode + " redirect from " + request
                .getURI() + " but no redirect location was specified");
        return false;
    }
}
