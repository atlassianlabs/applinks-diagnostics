package com.atlassian.applinks.diagnostics.tasks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Performs HTTP connectivity diagnosis to the remote application and verify if the remote application RPC URL is available
 */
public class HttpConnectivityRpcUrlDiagnosticTask extends AbstractApplinkDiagnosticTask
{
    // TODO i18n
    public static final String TEST_NAME = "Application URL HTTP Connectivity";
    public static final String URL = "1ojQIw";

    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages messages) {
        return this.diagnose(applicationLink, messages, Maps.<String, String>newHashMap());
    }

    @Override
    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages, Map<String, String> testParameters) {
        final TaskResult taskResult = new TaskResult(TEST_NAME);
        this.addKnowledgeBaseLink(taskResult);

        if (applicationLink == null)
        {
            taskResult.fail("Application Link is NULL");
            return false;
        }
        diagnosticMessages.addTaskResults(taskResult);

        if (applicationLink.getRpcUrl() == null)
        {
            taskResult.fail("Application URL is NULL");
            return false;
        }
        final HttpConnectivityTaskWorker taskWorker = new HttpConnectivityTaskWorker(applicationLink, taskResult, applicationLink.getRpcUrl());
        return taskWorker.runDiagnosis();
    }

    public String getName()
    {
        return TEST_NAME;
    }

    public String getURL() {
        return URL;
    }
}