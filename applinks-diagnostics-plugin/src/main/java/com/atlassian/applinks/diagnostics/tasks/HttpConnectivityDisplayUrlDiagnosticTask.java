package com.atlassian.applinks.diagnostics.tasks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Performs simple HTTP connectivity diagnosis to the remote application
 */
public class HttpConnectivityDisplayUrlDiagnosticTask extends AbstractApplinkDiagnosticTask
{
    // TODO i18n
    public static final String TEST_NAME = "Display URL HTTP Connectivity";
    public static final String URL = "zIjQIw";

    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages messages) {
        return this.diagnose(applicationLink, messages, Maps.<String, String>newHashMap());
    }

    @Override
    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages, Map<String, String> testParameters) {
        final TaskResult taskResult = new TaskResult(TEST_NAME);
        this.addKnowledgeBaseLink(taskResult);

        if (applicationLink == null)
        {
            taskResult.fail("Application Link is NULL");
            return false;
        }
        diagnosticMessages.addTaskResults(taskResult);

        if (applicationLink.getDisplayUrl() == null)
        {
            taskResult.fail("Display URL is NULL");
            return false;
        }
        final HttpConnectivityTaskWorker taskWorker = new HttpConnectivityTaskWorker(applicationLink, taskResult, applicationLink.getDisplayUrl());
        return taskWorker.runDiagnosis();
    }

    public String getName()
    {
        return TEST_NAME;
    }

    public String getURL() {
        return URL;
    }
}