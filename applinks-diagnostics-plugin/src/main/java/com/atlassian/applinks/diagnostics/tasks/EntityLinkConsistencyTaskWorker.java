package com.atlassian.applinks.diagnostics.tasks;

import java.util.List;
import java.util.Properties;
import java.util.UUID;

import com.atlassian.applinks.Utils.ApplicationLinkUtils;
import com.atlassian.applinks.Utils.EntityLinkUtils;
import com.atlassian.applinks.core.property.PropertyService;
import com.atlassian.applinks.diagnostics.TaskAction;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.applinks.fixes.AppLinkFixService;
import com.atlassian.applinks.fixes.tasks.CorrectPrimaryEntityLinks;
import com.atlassian.applinks.fixes.tasks.DeleteOrphanedEntityLinks;
import com.atlassian.applinks.host.spi.EntityReference;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import com.google.common.collect.Lists;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The task worker to verify the consistency of entityLinks data by looking at the stored pluginsettings data directly.
 *
 * @since v4.0.0
 */
public class EntityLinkConsistencyTaskWorker implements TaskWorker
{
    private static final Logger log = LoggerFactory.getLogger(EntityLinkConsistencyTaskWorker.class);

    private final PluginSettingsFactory pluginSettingsFactory;
    private final TypeAccessor typeAccessor;
    private final InternalHostApplication hostApplication;
    private final AppLinkFixService appLinkFixService;
    private final TaskResult taskResult;
    private final I18nResolver i18nResolver;
    private final PropertyService propertyService;

    private final List<String> missingParentIds = Lists.newArrayList();
    private final List<String> invalidPrimaryEntityReferences = Lists.newArrayList();

    public EntityLinkConsistencyTaskWorker(final PluginSettingsFactory pluginSettingsFactory,
            final TypeAccessor typeAccessor,
            final InternalHostApplication hostApplication,
            final AppLinkFixService appLinkFixService,
            final TaskResult taskResult,
            final I18nResolver i18nResolver,
            final PropertyService propertyService)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.typeAccessor = typeAccessor;
        this.hostApplication = hostApplication;
        this.appLinkFixService = appLinkFixService;
        this.taskResult = taskResult;
        this.i18nResolver = i18nResolver;
        this.propertyService = propertyService;
    }

    @Override
    public boolean runDiagnosis()
    {
        boolean pass = true;
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        // list of entities...
        for(EntityReference entityReference : this.hostApplication.getLocalEntities())
        {
            taskResult.info("Check EntityLinks for Entity [" + entityReference.getName() + "]");

            // TODO raise an error if the value returned wasn't a list....
            List<String> encodedLinks = EntityLinkUtils.getEncodedLinks(typeAccessor,pluginSettings, entityReference);
            if (encodedLinks == null)
            {
                taskResult.info("No EntityLinks for Entity [" + entityReference.getName() + "]");
                continue;
            }
            else
            {
                taskResult.info("Found EntityLinks " + encodedLinks.toString());
            }

            for (final String linkDefinition : encodedLinks)
            {
                if(!entityLinkIsValid(pluginSettings, linkDefinition, entityReference) && pass)
                {
                    pass = false;
                }
            }

            for(String encodedLinkString : encodedLinks)
            {
                final JSONObject encodedLink = getEncodedLinkJsonObject(encodedLinkString);

                if(encodedLink == null)
                {
                    continue;
                }

                String type = EntityLinkUtils.getJSONString(encodedLink, "type");
                String primaryLinkKey = getPrimaryLinkKey(typeAccessor, entityReference, type);
                Object primaryLinkObj = pluginSettings.get(primaryLinkKey);
                if(primaryLinkObj != null && primaryLinkObj instanceof Properties)
                {
                    if(!primaryKeyIsValid(pluginSettings, (Properties)primaryLinkObj, entityReference) && pass)
                    {
                        pass = false;
                    }
                }
                else if(primaryLinkObj != null)
                {
                    recordNeedForPrimaryFix(entityReference);
                    taskResult.error(String.format("Setting [%s] should be an instance of Properties but is an instance of [%s]", primaryLinkKey, primaryLinkObj.getClass().getCanonicalName()));
                    pass = false;
                }
                else
                {
                    recordNeedForPrimaryFix(entityReference);
                    taskResult.error(String.format("Setting [%s] is NULL or non existent", primaryLinkKey));
                    pass = false;
                }
            }
        }


        if(missingParentIds.size() > 0)
        {
            UUID actionUUID = this.appLinkFixService.addFixTask(new DeleteOrphanedEntityLinks(missingParentIds, this.pluginSettingsFactory, this.hostApplication, this.typeAccessor));
            // TODO this will not be comnplete in REST response....?
            taskResult.addAction(TaskAction.fix("/rest/applinksdiagnostics/1.0/fixApplicationLink/" + actionUUID, "Delete Orphaned Entity Links"));
        }

        if(invalidPrimaryEntityReferences.size() > 0)
        {
            UUID actionUUID = this.appLinkFixService.addFixTask(new CorrectPrimaryEntityLinks(invalidPrimaryEntityReferences, pluginSettingsFactory, hostApplication, typeAccessor, propertyService));
            // TODO this will not be comnplete in REST response....?
            taskResult.addAction(TaskAction.fix("/rest/applinksdiagnostics/1.0/fixApplicationLink/" + actionUUID, "Correct Primary Entity Links"));
        }

        if(!pass)
        {
            taskResult.fail("EntityLinks are not consistent");
        }
        else
        {
            taskResult.pass("Excellent EntityLinks are consistent! Nothing to see here.");
        }
        return pass;
    }

    public static String getPrimaryLinkKey(TypeAccessor typeAccessor, EntityReference entityReference, String type)
    {
        String entityLinkKey = EntityLinkUtils.getEntityKeyRoot(typeAccessor, entityReference);
        return entityLinkKey + "." + String.format("primary.%s", type);
    }

    public static JSONObject getEncodedLinkJsonObject(String encodedLinkString)
    {
        final JSONObject encodedLink;

        try
        {
            encodedLink = new JSONObject(encodedLinkString);
        }
        catch (JSONException e)
        {
            log.error("Unable to read JSON [%s]", encodedLinkString, e);
            return null;
        }
        return encodedLink;
    }

    private boolean primaryKeyIsValid(PluginSettings pluginSettings, Properties primaryLink, EntityReference entityReference)
    {
        final String localEntity = i18nResolver.getText(entityReference.getType().getI18nKey()) + ":" + entityReference.getKey() + "/" + entityReference.getName();
        boolean pass = true;
        if(!primaryLink.containsKey("applicationId"))
        {
            recordNeedForPrimaryFix(entityReference);
            taskResult.error(String.format("Primary EntityLink from [%s] is Invalid: missing applicationId", localEntity));
            pass = false;
        }
        else
        {
            String primaryApplicationId = primaryLink.getProperty("applicationId");
            boolean passParentCheck = parentApplicationLinkExists(pluginSettings, primaryApplicationId);

            if(!passParentCheck)
            {
                recordNeedForPrimaryFix(entityReference);
                taskResult.error(String.format("Primary EntityLink from [%s] is Orphaned: References ApplicationId [%s]", localEntity, primaryApplicationId));
            }
            pass = passParentCheck;
        }

        if(!primaryLink.containsKey("key"))
        {
            recordNeedForPrimaryFix(entityReference);
            taskResult.error(String.format("Primary EntityLink from [%s] is Invalid: missing key", localEntity));
            pass = false;
        }

        return pass;
    }

    private void recordNeedForPrimaryFix(EntityReference entityReference)
    {
        if(!invalidPrimaryEntityReferences.contains(CorrectPrimaryEntityLinks.getLocalEntityReferenceKey(entityReference)))
        {
            invalidPrimaryEntityReferences.add(CorrectPrimaryEntityLinks.getLocalEntityReferenceKey(entityReference));
        }
    }

    private boolean entityLinkIsValid(PluginSettings pluginSettings, String linkDefinition, EntityReference entityReference)
    {
        taskResult.info("Start validating EntityLink [" + linkDefinition + "]...");

        final JSONObject linkObject;

        try
        {
            linkObject = new JSONObject(linkDefinition);
        }
        catch (JSONException e)
        {
            taskResult.error("Failed to decode stored entity link to JSON [" + linkDefinition + "]");
            return false;
        }

        boolean pass = true;

        // configuration checks.
        final String applicationId = EntityLinkUtils.getJSONString(linkObject, ApplicationLinkUtils.APPLICATION_ID);
        final String typeI18n = EntityLinkUtils.getJSONString(linkObject, "typeI18n");
        final String key = EntityLinkUtils.getJSONString(linkObject, "key");
        final String name = EntityLinkUtils.getJSONString(linkObject, "name");
        final String localEntity = i18nResolver.getText(entityReference.getType().getI18nKey()) + ":" + entityReference.getKey() + "/" + entityReference.getName();
        final String remoteEntity = i18nResolver.getText(typeI18n) + ":" + key + "/" + name;
        pass = pass & parentApplicationLinkExists(pluginSettings, applicationId);

        if(!pass)
        {
            missingParentIds.add(applicationId);
            taskResult.error(String.format("EntityLink from [%s] to [%s] is Orphaned: References ApplicationId [%s]", localEntity, remoteEntity, applicationId));
        }

        taskResult.info("Completed validating EntityLink [" + linkDefinition + "]");

        return pass;
    }

    private boolean parentApplicationLinkExists(PluginSettings pluginSettings, String applicationId)
    {
        List<String> applicationIds  = (List<String>)pluginSettings.get("applinks.global.application.ids");

        if(!applicationIds.contains(applicationId))
        {
            return false;
        }
        return true;
    }
}
