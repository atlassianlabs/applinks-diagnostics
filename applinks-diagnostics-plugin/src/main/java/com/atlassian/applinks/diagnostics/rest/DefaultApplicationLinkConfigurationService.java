package com.atlassian.applinks.diagnostics.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.application.generic.GenericApplicationTypeImpl;
import com.atlassian.applinks.core.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.Link;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @since ShipIt23
 */
public class DefaultApplicationLinkConfigurationService implements ApplicationLinkConfigurationService {

    public static final String CONTEXT = "applicationlink";

    private final MutatingApplicationLinkService applicationLinkService;
    private final PluginAccessor pluginAccessor;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ServiceProviderStoreService serviceProviderStoreService;
    private final ConsumerService consumerService;

    public DefaultApplicationLinkConfigurationService(final MutatingApplicationLinkService applicationLinkService,
                                                      final PluginAccessor pluginAccessor,
                                                      final AuthenticationConfigurationManager authenticationConfigurationManager,
                                                      final ServiceProviderStoreService serviceProviderStoreService,
                                                      final ConsumerService consumerService)
    {
        this.applicationLinkService = applicationLinkService;
        this.pluginAccessor = pluginAccessor;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.serviceProviderStoreService = serviceProviderStoreService;
        this.consumerService = consumerService;
    }

    /**
     * Get filtered Configured providers for the current ApplicationLink.
     */
    public List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink, Predicate<AuthenticationProviderPluginModule> predicate)
            throws URISyntaxException
    {
        return getConfiguredProviders(applicationLink, Iterables.filter(pluginAccessor.getEnabledModulesByClass(AuthenticationProviderPluginModule.class), predicate));
    }

    /**
     * Get unfiltered Configured providers for the current ApplicationLink.
     */
    public List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink)
            throws URISyntaxException
    {
        return getConfiguredProviders(applicationLink, pluginAccessor.getEnabledModulesByClass(AuthenticationProviderPluginModule.class));
    }

    /**
     * Get Configured providers for the current ApplicationLink.
     */
    public List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink, Iterable<AuthenticationProviderPluginModule> pluginModules)
            throws URISyntaxException
    {
        // get the providers configured for this link
        final List<AuthenticationProviderEntity> configuredAuthProviders = new ArrayList<AuthenticationProviderEntity>();
        for (AuthenticationProviderPluginModule authenticationProviderPluginModule : pluginModules)
        {
            final AuthenticationProvider authenticationProvider = authenticationProviderPluginModule.getAuthenticationProvider(applicationLink);
            if (authenticationProvider != null)
            {
                Map<String, String> config = authenticationConfigurationManager.getConfiguration(applicationLink.getId(),authenticationProviderPluginModule.getAuthenticationProviderClass());
                configuredAuthProviders.add(new AuthenticationProviderEntity(
                        Link.self(new URI(CONTEXT + "/" + applicationLink.getId().toString() + "/authentication/provider")),
                        authenticationProviderPluginModule.getClass().getName(),
                        authenticationProviderPluginModule.getAuthenticationProviderClass().getName(),
                        config));
            }
        }
        return configuredAuthProviders;
    }

    /**
     * Find the ApplicationLink by its id.
     */
    public ApplicationLink findApplicationLink(final String id) throws TypeNotInstalledException
    {
        ApplicationId applicationId;
        try
        {
            applicationId = new ApplicationId(id);
        }
        catch(IllegalArgumentException e)
        {
            return null;
        }

        return applicationLinkService.getApplicationLink(applicationId);
    }

    /**
     * find an ApplicationLinks's Consumer
     */
    public Iterable<Consumer> findConsumers(ApplicationLink applicationLink, List<AuthenticationProviderEntity> configuredAuthProviders)
    {
        List<Consumer> consumers = Lists.newArrayList();
        Consumer consumer = serviceProviderStoreService.getConsumer(applicationLink);

        if(consumer != null)
        {
            // incoming consumer
            consumers.add(consumer);
        }

        // for generic applicationlinks the Consumer is stored in the OAuth ConsumerService using the consumerkey as the key
        // the consumerkey is stored as a property of the Authenticationprovider.
        if(applicationLink.getType() instanceof GenericApplicationTypeImpl)
        {
            for(AuthenticationProviderEntity entity : configuredAuthProviders)
            {
                if(applicationLink.getType() instanceof GenericApplicationTypeImpl)
                {
                    final String consumerKey = entity.getConfig().get("consumerKey.outbound");
                    if(StringUtils.isEmpty(consumerKey))
                    {
                        continue;
                    }

                    Consumer genericOutGoingConsumer = consumerService.getConsumerByKey(consumerKey);
                    if(genericOutGoingConsumer != null)
                    {
                        // outgoing consumer
                        consumers.add(genericOutGoingConsumer);
                    }
                }
            }
        }

        return consumers;
    }

    public List<AuthenticationProviderPluginModule> getConfiguredProviderPluginModules(ApplicationLink applicationLink, Predicate<AuthenticationProviderPluginModule> predicate)
    {
        return getConfiguredProviderPluginModules(applicationLink, Iterables.filter(pluginAccessor.getEnabledModulesByClass(AuthenticationProviderPluginModule.class), predicate));
    }

    private List<AuthenticationProviderPluginModule> getConfiguredProviderPluginModules(ApplicationLink applicationLink, Iterable<AuthenticationProviderPluginModule> pluginModules)
    {
            // get the providers configured for this link
        final List<AuthenticationProviderPluginModule> configuredAuthProviders = new ArrayList<AuthenticationProviderPluginModule>();
        for (AuthenticationProviderPluginModule authenticationProviderPluginModule : pluginModules)
        {
            final AuthenticationProvider authenticationProvider = authenticationProviderPluginModule.getAuthenticationProvider(applicationLink);
            if (authenticationProvider != null)
            {
                configuredAuthProviders.add(authenticationProviderPluginModule);
            }
        }
        return configuredAuthProviders;
    }

    public boolean isConfigured(ApplicationLink applicationLink, AuthenticationProviderPluginModule authenticationProviderPluginModule)
    {
        if(authenticationProviderPluginModule.getAuthenticationProvider(applicationLink) != null)
        {
            return authenticationConfigurationManager.isConfigured(applicationLink.getId(), authenticationProviderPluginModule.getAuthenticationProviderClass());
        }
        return false;
    }

}
