package com.atlassian.applinks.diagnostics.tasks;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.UUID;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.application.bamboo.BambooApplicationTypeImpl;
import com.atlassian.applinks.application.confluence.ConfluenceApplicationTypeImpl;
import com.atlassian.applinks.application.fecru.FishEyeCrucibleApplicationTypeImpl;
import com.atlassian.applinks.application.jira.JiraApplicationTypeImpl;
import com.atlassian.applinks.application.refapp.RefAppApplicationTypeImpl;
import com.atlassian.applinks.application.stash.StashApplicationTypeImpl;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskAction;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.applinks.diagnostics.rest.ApplicationLinkConfigurationService;
import com.atlassian.applinks.fixes.AppLinkFixService;
import com.atlassian.applinks.fixes.tasks.FixBaseUrlTask;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.oauth.Consumer;
import com.atlassian.plugin.PluginAccessor;

import com.google.common.collect.Lists;

import static java.lang.String.format;

/**
 * @since ShipIt23
 *
 * Validate the application link manifest from the remote application
 */
public class ValidateRemoteManifestTask extends AbstractApplinkDiagnosticTask
{
    private final ManifestRetriever manifestRetriever;
    private final AppLinkPluginUtil pluginUtils;
    private final AppLinkFixService appLinkFixService;
    private final MutatingApplicationLinkService applicationLinkService;
    public static final String TEST_NAME = "Validate Remote Manifest";
    public static final String URL = "3ojQIw";
    private final PluginAccessor pluginAccessor;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ApplicationLinkConfigurationService applicationLinkConfigurationService;


    public ValidateRemoteManifestTask(final ManifestRetriever manifestRetriever,
                                      final AppLinkPluginUtil pluginUtils,
                                      final AppLinkFixService appLinkFixService,
                                      final MutatingApplicationLinkService applicationLinkService,
                                      final PluginAccessor pluginAccessor,
                                      final AuthenticationConfigurationManager authenticationConfigurationManager,
                                      final ApplicationLinkConfigurationService applicationLinkConfigurationService)
    {
        this.manifestRetriever = manifestRetriever;
        this.pluginUtils = pluginUtils;
        this.appLinkFixService = appLinkFixService;
        this.applicationLinkService = applicationLinkService;
        this.pluginAccessor = pluginAccessor;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.applicationLinkConfigurationService = applicationLinkConfigurationService;
    }

    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages) {
        final TaskResult taskResult = new TaskResult(TEST_NAME);
        this.addKnowledgeBaseLink(taskResult);

        if (applicationLink == null)
        {
            taskResult.fail("Application Link is NULL");
            diagnosticMessages.addTaskResults(taskResult);
            return false;
        }

        final URI rpcUrl = applicationLink.getRpcUrl();

        if (rpcUrl == null)
        {
            taskResult.fail("Application URL is NULL");
            diagnosticMessages.addTaskResults(taskResult);
            return false;
        }

        taskResult.details(format("Check that %s is the application we think it is by comparing its configuration with our record of it.", applicationLink.getName()));


        String requestUrl = rpcUrl.toString();

        URI requestUri;
        try
        {
            requestUri = new URI(requestUrl);
        }
        catch(URISyntaxException ex)
        {
            taskResult.error(String.format("Oops the Application URL '%s' invalid", requestUrl));
            diagnosticMessages.addTaskResults(taskResult);
            return false;
        }

        Manifest manifest;
        try {
            manifest = getManifestViaManifestRetriever(requestUri);
        } catch (ManifestNotFoundException e) {
            taskResult.fail(String.format("Oh dear we're unable to retrieve the configuration from '%s'", e.getMessage()));
            diagnosticMessages.addTaskResults(taskResult);
            return false;
        }

        boolean validationResult = this.validateManifest(taskResult, applicationLink, manifest);

        if(validationResult)
        {
            taskResult.pass("Sucessfully validated the manifest in " + applicationLink.getName());
        }

        diagnosticMessages.addTaskResults(taskResult);
        return validationResult;
    }

    private boolean validateManifest(TaskResult taskResult, ApplicationLink applicationLink, Manifest manifest)
    {
        boolean valid = true;
        if(!manifest.getId().equals(applicationLink.getId()))
        {
            taskResult.warning(String.format("You know this should be impossible but the local Application Link ID '%s' does not match '%s' 's ID '%s'", applicationLink.getId(), manifest.getName(), manifest.getId()));
            valid = false;
        }

        if(!manifest.getUrl().equals(applicationLink.getRpcUrl()))
        {
            taskResult.warning(String.format("Aha! The Application Link's Application URL '%s' does not match '%s' 's BASE URL '%s'. We can fix this!", applicationLink.getRpcUrl(), manifest.getName(), manifest.getUrl()));
            UUID actionUUID = this.appLinkFixService.addFixTask(new FixBaseUrlTask(applicationLink.getId(), applicationLinkService, manifestRetriever));
            //TODO the context shoudl be taken care of by the client.
            taskResult.addAction(TaskAction.fix("/refapp/rest/applinksdiagnostics/1.0/fixApplicationLink/" + actionUUID, "Correct Base Url"));
            valid = false;
        }
// TODO need access to felix
//        if(!manifest.getAppLinksVersion().equals(this.pluginUtils.getVersion()))
//        {
//            taskResult.warning(String
//                    .format("The local and '%s' 's versions of Application Links are out of sync. '%s' vs '%s'.", manifest.getName(), pluginUtils
//                            .getVersion(), manifest.getAppLinksVersion()));
//            taskResult.addAction(KnowledgeBaseArticles.APPLINK_VERSION_MISMATCH.toAction());
//            valid = false;
//        }

        if( manifest.hasPublicSignup()
                && this.twoLOImpersontationAllowed(applicationLink))
        {
            taskResult.error(String
                    .format("This might be important! The local Application allows '%s' 's impersonating access while it allows public sign on.", manifest.getName()));
            valid = false;
        }
        return valid;
    }

    private Boolean twoLOImpersontationAllowed(ApplicationLink applicationLink) {
        try {
            Iterable<Consumer>  consumers = applicationLinkConfigurationService.findConsumers(applicationLink, applicationLinkConfigurationService.getConfiguredProviders(applicationLink));
            if(consumers.iterator().hasNext())
            {
                Consumer consumer = consumers.iterator().next();
                return consumer.getTwoLOImpersonationAllowed();
            }
            return false;
        } catch (URISyntaxException e) {
            return false;
        }
    }

    private Manifest getManifestViaManifestRetriever(URI requestUri) throws ManifestNotFoundException {
        return manifestRetriever.getManifest(requestUri);
    }

    public ArrayList<String> getTargetApplicationTypes() {
        // I know this is shit but...
        return Lists.newArrayList(
                new BambooApplicationTypeImpl(null, null).getI18nKey(),
                new ConfluenceApplicationTypeImpl(null, null).getI18nKey(),
                new FishEyeCrucibleApplicationTypeImpl(null, null).getI18nKey(),
                new JiraApplicationTypeImpl(null, null).getI18nKey(),
                new RefAppApplicationTypeImpl(null, null).getI18nKey(),
                new StashApplicationTypeImpl(null, null).getI18nKey()
        );
    }

    public String getName() {
        return TEST_NAME;
    }

    public String getURL() {
        return URL;
    }
}
