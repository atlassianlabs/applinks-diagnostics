package com.atlassian.applinks.diagnostics.tasks;

import java.util.ArrayList;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.application.bamboo.BambooApplicationTypeImpl;
import com.atlassian.applinks.application.confluence.ConfluenceApplicationTypeImpl;
import com.atlassian.applinks.application.fecru.FishEyeCrucibleApplicationTypeImpl;
import com.atlassian.applinks.application.jira.JiraApplicationTypeImpl;
import com.atlassian.applinks.application.refapp.RefAppApplicationTypeImpl;
import com.atlassian.applinks.application.stash.StashApplicationTypeImpl;
import com.atlassian.applinks.core.auth.oauth.OAuthTokenRetriever;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.message.I18nResolver;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Performs authentication diagnosis, to see if an application is able to authenticate to the remote application via applink
 */
public class AuthenticationDiagnosticTask extends AbstractApplinkDiagnosticTask
{
    public static final String TEST_NAME = "Authentication Check";
    public static final String URL = "w4jQIw";

    private final PluginAccessor pluginAccessor;
    private final AuthenticationConfigurationManager authConfigurationManager;
    private final OAuthTokenRetriever oAuthTokenRetriever;
    private final ConsumerService consumerService;
    private final I18nResolver i18nResolver;
    private final HostApplication hostApplication;

    public AuthenticationDiagnosticTask(PluginAccessor pluginAccessor, AuthenticationConfigurationManager authConfigurationManager, OAuthTokenRetriever oAuthTokenRetriever, ConsumerService consumerService, I18nResolver i18nResolver, HostApplication hostApplication)
    {
        this.pluginAccessor = pluginAccessor;
        this.authConfigurationManager = authConfigurationManager;
        this.oAuthTokenRetriever = oAuthTokenRetriever;
        this.consumerService = consumerService;
        this.i18nResolver = i18nResolver;
        this.hostApplication = hostApplication;
    }

    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages messages)
    {
        final TaskResult taskResult = new TaskResult(TEST_NAME);
        this.addKnowledgeBaseLink(taskResult);

        if (applicationLink == null)
        {
            taskResult.fail("Application Link is NULL");
            return false;
        }
        messages.addTaskResults(taskResult);

        if (applicationLink.getRpcUrl() == null)
        {
            taskResult.fail("Application URL is NULL");
            return false;
        }
        final AuthenticationTaskWorker taskWorker = new AuthenticationTaskWorker(
                pluginAccessor, authConfigurationManager, oAuthTokenRetriever, consumerService, applicationLink, taskResult, i18nResolver, hostApplication);
        return taskWorker.runDiagnosis();
    }

    public ArrayList<String> getTargetApplicationTypes()
    {
        // I know this is shit but...
        return newArrayList(
                new BambooApplicationTypeImpl(null, null).getI18nKey(),
                new ConfluenceApplicationTypeImpl(null, null).getI18nKey(),
                new FishEyeCrucibleApplicationTypeImpl(null, null).getI18nKey(),
                new JiraApplicationTypeImpl(null, null).getI18nKey(),
                new RefAppApplicationTypeImpl(null, null).getI18nKey(),
                new StashApplicationTypeImpl(null, null).getI18nKey()
        );
    }

    public String getName()
    {
        return TEST_NAME;
    }

    public String getURL() {
        return URL;
    }
}
