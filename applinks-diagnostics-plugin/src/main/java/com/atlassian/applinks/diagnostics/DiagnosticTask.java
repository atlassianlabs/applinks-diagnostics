package com.atlassian.applinks.diagnostics;

import java.util.ArrayList;

public interface DiagnosticTask
{
    /**
     * Get the target application types for this task
     */
    ArrayList<String> getTargetApplicationTypes();

    /**
     * Get the task name.
     * @return
     */
    String getName();

    /**
     * Get the tiny URL.
     * @return
     */
    String getURL();

    /**
     * Set the test scope, per "link" or "global"
     * defaults to link
     * @param scope
     */
    void setScope(String scope);
    String getScope();
}
