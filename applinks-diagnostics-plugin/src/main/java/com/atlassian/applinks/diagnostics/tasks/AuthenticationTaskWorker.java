package com.atlassian.applinks.diagnostics.tasks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.core.auth.oauth.OAuthTokenRetriever;
import com.atlassian.applinks.core.auth.oauth.ServiceProviderUtil;
import com.atlassian.applinks.core.auth.oauth.servlets.consumer.AddServiceProviderManuallyServlet;
import com.atlassian.applinks.diagnostics.KnowledgeBaseArticles;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.applinks.host.spi.HostApplication;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.atlassian.applinks.diagnostics.TaskAction.authenticate;
import static java.lang.String.format;

/**
 * Task worker for running authentication diagnosis/tests
 * This worker will try to authenticate against the remote application, and also check the authentication strategy (trusted, OAuth, basic)
 */
public class AuthenticationTaskWorker
{
    private static final Logger log = LoggerFactory.getLogger(AuthenticationTaskWorker.class);

    private static final String RPC_PATH = "plugins/servlet/applinksDiagnostics/authenticate";

    private final PluginAccessor pluginAccessor;
    private final AuthenticationConfigurationManager authConfigurationManager;
    private final OAuthTokenRetriever oAuthTokenRetriever;
    private final ConsumerService consumerService;

    private final ApplicationLink applicationLink;
    private final TaskResult taskResult;
    private final I18nResolver i18nResolver;
    private final HostApplication hostApplication;

    public AuthenticationTaskWorker(PluginAccessor pluginAccessor, AuthenticationConfigurationManager authConfigurationManager,
                                    OAuthTokenRetriever oAuthTokenRetriever, ConsumerService consumerService,
                                    ApplicationLink applicationLink, TaskResult taskResult,
                                    I18nResolver i18nResolver, HostApplication hostApplication)
    {
        this.pluginAccessor = pluginAccessor;
        this.authConfigurationManager = authConfigurationManager;
        this.oAuthTokenRetriever = oAuthTokenRetriever;
        this.consumerService = consumerService;
        this.applicationLink = applicationLink;
        this.taskResult = taskResult;
        this.i18nResolver = i18nResolver;
        this.hostApplication = hostApplication;
    }

    public boolean runDiagnosis()
    {
        taskResult.info(format("Attempting authentication of link to %s", applicationLink.getName()));
        final String baseRequestUrl = buildInitialRestUrl(applicationLink);

        boolean connectionSuccess = true;
        List<String> failedProviders = new ArrayList<String>();
        int providerCount = 0;
        for (Class<? extends AuthenticationProvider> authenticationProvider : getAuthProviderDescriptors())
        {
            final AuthStrategy authStrategy = getAuthStrategy(authenticationProvider);
            taskResult.info("Checking if " + authStrategy + " is configured");

            if (authStrategy.isConfigured())
            {
                String message = authStrategy + " is configured for " + applicationLink.getName() + ", trying authentication";
                log.debug(message);
                taskResult.info(message);
                providerCount++;

                boolean connectionResult = authStrategy.attemptAuthentication(baseRequestUrl);
                if (!connectionResult) {
                    connectionSuccess = false;
                    failedProviders.add(authenticationProvider.getSimpleName());
                }
            }
            else
            {
                String message = authStrategy + " is not configured for " + applicationLink.getName() + ", skipping";
                log.debug(message);
                taskResult.info(message);
            }
        }

        return connectionSuccess || failedToEstablishConnection(failedProviders, providerCount);
    }

    private boolean failedToEstablishConnection(List<String> failedProviders, int providerCount)
    {
        String message = "Failed to authenticate against " + applicationLink.getName() + " using:";
        for (String failedProvider : failedProviders) {
            message += " " + failedProvider;
        }

        if (failedProviders.size() >= providerCount)
        {
            taskResult.fail(message);
        } else {
            taskResult.warning(message);
        }
        return false;
    }

    private Iterable<Class<? extends AuthenticationProvider>> getAuthProviderDescriptors()
    {
        // TODO get these dynamically via pluginAccessor
//        final List<AuthenticationProviderModuleDescriptor> moduleDescriptorsX = pluginAccessor
//                .getEnabledModuleDescriptorsByClass(AuthenticationProviderModuleDescriptor.class);
//        Collections.sort(moduleDescriptorsX, AuthenticationProviderModuleDescriptor.BY_WEIGHT);
        final List<Class<? extends AuthenticationProvider>> moduleDescriptors = Lists.newArrayList(
                OAuthAuthenticationProvider.class,
                TwoLeggedOAuthAuthenticationProvider.class,
                TwoLeggedOAuthWithImpersonationAuthenticationProvider.class,
                TrustedAppsAuthenticationProvider.class,
                BasicAuthenticationProvider.class);
        return moduleDescriptors;
    }

    private String buildInitialRestUrl(ApplicationLink link)
    {
        String url = link.getRpcUrl().toString();
        if (!url.endsWith("/"))
        {
            return url + '/' + RPC_PATH;
        }
        else
        {
            return url + RPC_PATH;
        }
    }

    AuthStrategy getAuthStrategy(Class<? extends AuthenticationProvider> authProviderClass)
    {
        if (authProviderClass.getSimpleName().equals("OAuthAuthenticationProvider"))
        {
            return new OAuthDancingStrategy(i18nResolver.getText(authProviderClass.getSimpleName()), authProviderClass, new DefaultResponseHandler(this.taskResult));
        }

        return new AuthStrategy(i18nResolver.getText(authProviderClass.getSimpleName()), authProviderClass, new DefaultResponseHandler(this.taskResult));
    }

    private class AuthStrategy
    {
        private final String name;
        protected final Class<? extends AuthenticationProvider> authProviderClass;
        private final ResponseHandler<Response> responseHandler;

        AuthStrategy(String name, Class<? extends AuthenticationProvider> authProviderClass, ResponseHandler<Response> responseHandler)
        {
            this.name = name;
            this.authProviderClass = authProviderClass;
            this.responseHandler = responseHandler;
        }

        protected Map<String, String> getAuthConfig()
        {
            return authConfigurationManager
                    .getConfiguration(applicationLink.getId(), authProviderClass);
        }

        boolean attemptAuthentication(final String baseUrl)
        {
            final boolean authenticated = performAuthenticatedRequest(baseUrl);
            if (!authenticated)
            {
                return handleAuthenticationFailure();
            }
            else
            {
                return successfullyConnectedAuthenticated();
            }
        }

        protected boolean handleAuthenticationFailure()
        {
            return cannotAuthenticate();
        }

        private boolean cannotAuthenticate()
        {
            taskResult.error("Failed to connect using " + this + " to " + applicationLink.getName());
            return false;
        }

        private boolean performAuthenticatedRequest(String uri)
        {
            final ApplicationLinkRequestFactory requestFactory = applicationLink
                    .createAuthenticatedRequestFactory(authProviderClass);
            try
            {
                taskResult.info("Attempting to request " + uri );
                final ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, uri);
                // TODO pass in bespoke response handler to view any errors.
                request.execute(this.responseHandler);
                return true;
            }
            catch (ResponseException ex)
            {
                log.error(this + " failed: " + ex.getMessage());
                return false;
            }
            catch (CredentialsRequiredException ex)
            {
                log.error(this + " failed: " + ex.getMessage());
                return false;
            }
        }

        public boolean isConfigured()
        {
//            if (authProviderClass.getAuthenticationProvider(applicationLink) != null)
  //          {
                return authConfigurationManager
                        .isConfigured(applicationLink.getId(), authProviderClass);
    //        }
      //      return false;
        }


        private boolean successfullyConnectedAuthenticated()
        {
            taskResult.pass(format("Successfully connected with %s to %s", this, applicationLink.getName()));
            return true;
        }

        @Override
        public String toString()
        {
            return name;
        }

        public ResponseHandler<Response> getResponseHandler()
        {
            return responseHandler;
        }
    }

    class OAuthDancingStrategy extends AuthStrategy
    {
        OAuthDancingStrategy(String name, Class<? extends AuthenticationProvider> authProviderClass, DefaultResponseHandler responseHandler)
        {
            super(name, authProviderClass, responseHandler);
        }

        @Override
        protected boolean handleAuthenticationFailure()
        {
            if (verifyOAUthToken())
            {
                taskResult.error("User auth required by " + this);
                final ApplicationLinkRequestFactory requestFactory = applicationLink
                        .createAuthenticatedRequestFactory(authProviderClass);
                taskResult.addAction(authenticate(
                        applicationLink.getId().toString(),
                        applicationLink.getName(),
                        applicationLink.getRpcUrl().toString(),
                        requestFactory.getAuthorisationURI().toString(),
                        "User auth required by " + this));
                return false;
            }
            else
            {
                taskResult.error("Failed to connect using " + this + " to " + applicationLink.getName());
                return false;
            }
        }

        private boolean verifyOAUthToken()
        {
            ServiceProvider serviceProvider = ServiceProviderUtil.getServiceProvider(getAuthConfig(), applicationLink);
            try
            {
                oAuthTokenRetriever.getRequestToken(serviceProvider, getConsumerKey(), "http://dummy.x");
                return true;
            }
            catch (ResponseException e)
            {
                log.debug("Failed to retrieve OAuth request token from {}: {}", applicationLink.getName(), e
                        .getMessage());
                taskResult.warning(this + " " + e.getMessage());
                return false;
            }
        }

        private String getConsumerKey()
        {
            final Map<String, String> config = authConfigurationManager
                    .getConfiguration(applicationLink.getId(), OAuthAuthenticationProvider.class);
            if (config.containsKey(AddServiceProviderManuallyServlet.CONSUMER_KEY_OUTBOUND))
            {
                return config.get(AddServiceProviderManuallyServlet.CONSUMER_KEY_OUTBOUND);
            }
            return consumerService.getConsumer().getKey();
        }
    }
}