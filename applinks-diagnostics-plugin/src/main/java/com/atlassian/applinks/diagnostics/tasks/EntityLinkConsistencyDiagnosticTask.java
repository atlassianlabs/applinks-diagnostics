package com.atlassian.applinks.diagnostics.tasks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.core.property.PropertyService;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskAction;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.applinks.fixes.AppLinkFixService;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Task for verifying the EntityLink data, to check if it's consistent
 */
public class EntityLinkConsistencyDiagnosticTask extends AbstractApplinkDiagnosticTask
{
    private static final Logger log = LoggerFactory.getLogger(EntityLinkConsistencyDiagnosticTask.class);

    public static final String TEST_NAME = "EntityLink Consistency";
    public static final String URL = "xojQIw";
    private final PluginSettingsFactory pluginSettingsFactory;
    private final TypeAccessor typeAccessor;
    private final InternalHostApplication internalHostApplication;
    private final AppLinkFixService appLinkFixService;
    private final I18nResolver i18nResolver;
    private final PropertyService propertyService;


    public EntityLinkConsistencyDiagnosticTask(final PluginSettingsFactory pluginSettingsFactory,
            final TypeAccessor typeAccessor,
            final InternalHostApplication internalHostApplication,
            final AppLinkFixService appLinkFixService,
            final I18nResolver i18nResolver,
            final PropertyService propertyService)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.typeAccessor = typeAccessor;
        this.internalHostApplication = internalHostApplication;
        this.appLinkFixService = appLinkFixService;
        this.i18nResolver = i18nResolver;
        this.propertyService = propertyService;
    }

    @Override
    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages)
    {
        // this is a global task that works across all application links.
        Preconditions.checkArgument(applicationLink == null);

        // TODO boilerplate
        final TaskResult taskResult = new TaskResult(getName());
        addKnowledgeBaseLink(taskResult);

        diagnosticMessages.addTaskResults(taskResult);

        taskResult.details("Testing consistency of all Entity Links data");
        final EntityLinkConsistencyTaskWorker taskWorker = new EntityLinkConsistencyTaskWorker(this.pluginSettingsFactory, this.typeAccessor, this.internalHostApplication, this.appLinkFixService, taskResult, this.i18nResolver, this.propertyService);
        return taskWorker.runDiagnosis();
    }

    @Override
    public String getName()
    {
        return TEST_NAME;
    }

    public String getURL() {
        return URL;
}
}
