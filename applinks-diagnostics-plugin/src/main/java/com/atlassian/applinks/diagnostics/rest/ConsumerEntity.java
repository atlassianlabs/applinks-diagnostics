package com.atlassian.applinks.diagnostics.rest;

/**
 * Represents the configuration of a Consumer of services provided by the Host via an ApplicationLink
 *
 * @since v3.12
 */

import com.atlassian.applinks.core.rest.model.LinkedEntity;
import com.atlassian.applinks.core.rest.model.adapter.OptionalURIAdapter;
import com.atlassian.oauth.Consumer;
import com.atlassian.plugins.rest.common.Link;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.net.URI;

@XmlRootElement (name = "consumer")
public class ConsumerEntity extends LinkedEntity
{
    @XmlElement (name = "key")
    private String key;
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "description")
    private String description;
    @XmlElement(name = "signatureMethod")
    private Consumer.SignatureMethod signatureMethod;
    @XmlElement(name = "publicKey")
    private String publicKey;
    @XmlElement(name = "sharedSecret")
    private String sharedSecret;
    @XmlJavaTypeAdapter (OptionalURIAdapter.class)
    @XmlElement(name = "callback")
    private URI callback;
    @XmlElement(name = "twoLOAllowed")
    private Boolean twoLOAllowed;
    @XmlElement(name = "executingTwoLOUser")
    private String executingTwoLOUser;
    @XmlElement(name = "twoLOImpersonationAllowed")
    private Boolean twoLOImpersonationAllowed;
    @XmlElement(name = "outgoing")
    private Boolean outgoing;

    public ConsumerEntity()
    {
    }

    public ConsumerEntity(final Link self, final Consumer consumer)
    {
        if( consumer == null )
        {
            return;
        }

        this.key = consumer.getKey();
        this.name = consumer.getName();
        this.description = consumer.getDescription();
        this.signatureMethod = consumer.getSignatureMethod();
        if(consumer.getPublicKey() != null)
        {
            this.publicKey = consumer.getPublicKey().toString();
        }
        this.callback = consumer.getCallback();
        this.twoLOAllowed = consumer.getTwoLOAllowed();
        this.executingTwoLOUser = consumer.getExecutingTwoLOUser();
        this.twoLOImpersonationAllowed = consumer.getTwoLOImpersonationAllowed();

        addLink(self);
    }

    @Nullable
    public String getKey()
    {
        return key;
    }

    @Nullable
    public String getName()
    {
        return name;
    }

    @Nullable
    public String getDescription()
    {
        return description;
    }

    @Nullable
    public Consumer.SignatureMethod getSignatureMethod()
    {
        return signatureMethod;
    }

    @Nullable
    public String getPublicKey()
    {
        return publicKey;
    }

    @Nullable
    public URI getCallback()
    {
        return callback;
    }

    public boolean isTwoLOAllowed()
    {
        return twoLOAllowed;
    }

    @Nullable
    public String getExecutingTwoLOUser()
    {
        return executingTwoLOUser;
    }

    public boolean isTwoLOImpersonationAllowed()
    {
        return twoLOImpersonationAllowed;
    }

    @Nullable
    public String getSharedSecret()
    {
        return sharedSecret;
    }

    public boolean isOutgoing()
    {
        return outgoing;
    }
}
