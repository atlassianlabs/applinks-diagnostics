package com.atlassian.applinks.diagnostics.tasks;

import java.util.Map;

import com.atlassian.applinks.diagnostics.TaskAction;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;

import org.apache.commons.lang.StringEscapeUtils;

public class DefaultResponseHandler implements ResponseHandler<Response> {
    private final TaskResult taskResult;

    public DefaultResponseHandler(TaskResult taskResult) {

        this.taskResult = taskResult;
    }

    @Override
    public void handle(Response response) throws ResponseException {
        recordResponse(response, this.taskResult);

        if(response.getStatusCode() < 200) {
            taskResult.info("Successfully connected: Information");
        } else if(response.getStatusCode() < 300) {
            taskResult.info("Successfully connected");
        } else if (response.getStatusCode() < 400) {
            taskResult.warning("Failed to connect: Redirection");
            throw new ResponseException();
        } else if (response.getStatusCode() == 401) {
            taskResult.warning("Failed to connect: Unauthorized");
            throw new ResponseException();
        } else if (response.getStatusCode() == 404) {
            taskResult.warning("Failed to connect: Not Found");
            taskResult.addAction(TaskAction.install());
            throw new ResponseException();
        } else if (response.getStatusCode() < 500) {
            taskResult.warning("Failed to connect: Client Error");
            throw new ResponseException();
        } else {
            taskResult.warning("Failed to connect: Server Error");
            throw new ResponseException();
        }
    }

    public static void recordResponse(Response response, TaskResult taskResult) throws ResponseException {
        recordResponse(response.getStatusCode(), response.getHeaders(), response.getResponseBodyAsString(), taskResult);
    }

    public static void recordResponse(int statusCode, Map<String, String> headers, String body, TaskResult taskResult) {
        taskResult.info("Response Status:" + statusCode);
        //taskResult.info("Response Body:" + StringEscapeUtils.escapeHtml(body));
        for(String name : headers.keySet()) {
            taskResult.info("Response Header:" + StringEscapeUtils.escapeHtml(name) + "=" + StringEscapeUtils.escapeHtml(headers.get(name)));
        }
    }
}
