package com.atlassian.applinks.diagnostics;

public enum KnowledgeBaseArticles
{
    TOO_MANY_REDIRECTS("https://pug.jira.com/wiki/display/~kmacleod/Too+Many+Redirects", "Too Many Redirects"),
    APPLINK_VERSION_MISMATCH("https://pug.jira.com/wiki/display/~kmacleod/AppLink+Version+Mismatch", "AppLink Version Mismatch"),
    AUTH_FAIL("https://pug.jira.com/wiki/display/~kmacleod/Auth+Fail", "Auth Fail");

    private final String url;
    private final String description;

    KnowledgeBaseArticles(final String url, final String description)
    {
        this.url = url;
        this.description = description;
    }

    public TaskAction toAction()
    {
        return TaskAction.knowledgeBase(url, description);
    }
}
