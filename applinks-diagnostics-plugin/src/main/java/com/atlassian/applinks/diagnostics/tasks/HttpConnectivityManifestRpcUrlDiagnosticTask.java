package com.atlassian.applinks.diagnostics.tasks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.google.common.collect.Maps;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Performs HTTP connectivity diagnosis to the remote application to retrieve the application link manifest from the remote application
 */
public class HttpConnectivityManifestRpcUrlDiagnosticTask extends AbstractApplinkDiagnosticTask
{
    // TODO i18n
    public static final String TEST_NAME = "Manifest Application URL HTTP Connectivity";
    public static final String URL = "0ojQIw";

    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages messages) {
        return this.diagnose(applicationLink, messages, Maps.<String, String>newHashMap());
    }

    @Override
    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages, Map<String, String> testParameters) {
        final TaskResult taskResult = new TaskResult(TEST_NAME);
        this.addKnowledgeBaseLink(taskResult);

        if (applicationLink == null)
        {
            taskResult.fail("Application Link is NULL");
            return false;
        }
        diagnosticMessages.addTaskResults(taskResult);

        if (applicationLink.getRpcUrl() == null)
        {
            taskResult.fail("Application URL is NULL");
            return false;
        }
        final HttpConnectivityTaskWorker taskWorker;
        try {
            taskWorker = new HttpConnectivityTaskWorker(applicationLink, taskResult, new URI(applicationLink.getDisplayUrl().toASCIIString() + "/rest/applinks/1.0/manifest"));
        } catch (URISyntaxException e) {
            taskResult.fail("Manifest URL is invalid");
            return false;
        }
        return taskWorker.runDiagnosis();
    }

    public String getName()
    {
        return TEST_NAME;
    }

    public String getURL() {
        return URL;
    }

}