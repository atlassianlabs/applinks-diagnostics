package com.atlassian.applinks.diagnostics.tasks;

import java.io.IOException;
import java.util.ArrayList;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.application.confluence.ConfluenceApplicationTypeImpl;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskResult;

import com.google.common.collect.Lists;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;

/**
 * @since ShipIt23
 *
 * Validate if the remote API call is enabled on the remote instance
 * Currently disabled
 */
public class ValidateRemoteConfluenceRPCActiveTask extends AbstractApplinkDiagnosticTask
{
    // TODO i18n
    public static final String TEST_NAME = "Confluence RPC Enabled";
    public static final String URL = "4IjQIw";

    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages) {
        TaskResult taskResult = new TaskResult(this.getName());
        taskResult.details(String.format("If '%s' 's RPCs are disabled it could cause problems when using the Application Link.", applicationLink.getName()));

        Boolean result = false;
        HttpClient client = new HttpClient();
        HttpMethod getMethod = new GetMethod(applicationLink.getRpcUrl() + "/rpc/xmlrpc");
        try {
            final int statusCode = client.executeMethod(getMethod);
            if(statusCode == 200)
            {
                taskResult.pass(String.format("Great the RPCs are active on '%s'", applicationLink.getName()));
                result = true;
            }
            else
            {
                taskResult.fail(String.format("Oh the RPC's appear to be disabled on '%s'", applicationLink.getName()));
                result = false;
            }
        } catch (IOException e) {
            taskResult.error(e);
        }
        diagnosticMessages.addTaskResults(taskResult);
        return result;
    }

    public ArrayList<String> getTargetApplicationTypes() {
        // I know this is shit but...
        return Lists.newArrayList(
            new ConfluenceApplicationTypeImpl(null, null).getI18nKey()
        );
    }

    public String getName()
    {
        return TEST_NAME;
    }

    public String getURL() {
        return URL;
    }
}
