package com.atlassian.applinks.diagnostics.tasks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.application.bamboo.BambooApplicationTypeImpl;
import com.atlassian.applinks.application.confluence.ConfluenceApplicationTypeImpl;
import com.atlassian.applinks.application.fecru.FishEyeCrucibleApplicationTypeImpl;
import com.atlassian.applinks.application.generic.GenericApplicationTypeImpl;
import com.atlassian.applinks.application.jira.JiraApplicationTypeImpl;
import com.atlassian.applinks.application.refapp.RefAppApplicationTypeImpl;
import com.atlassian.applinks.application.stash.StashApplicationTypeImpl;
import com.atlassian.applinks.diagnostics.AppLinkDiagnosticTask;
import com.atlassian.applinks.diagnostics.DiagnosticMessages;
import com.atlassian.applinks.diagnostics.TaskAction;
import com.atlassian.applinks.diagnostics.TaskResult;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;

import static com.atlassian.applinks.diagnostics.TaskAction.authenticate;

/**
 * @since ShipIt23
 *
 * Abstract class for authentication tests (two and three legged OAuth, two legged impersonation OAuth)
 * As the OAuth tests are currently disabled (see atlassian-plugin.xml), this class is currently unused
 */
public abstract class AbstractApplinkDiagnosticTask implements AppLinkDiagnosticTask
{
    private static final Logger log = LoggerFactory.getLogger(AbstractApplinkDiagnosticTask.class);
    private String scope;

    public boolean diagnose(TaskResult taskResult, ApplicationLink applicationLink)
    {
        if (applicationLink == null)
        {
            taskResult.fail("Application Link is NULL");
            return false;
        }
        return true;
    }

    protected String performAuthenticatedRequest(ApplicationLink applicationLink, AuthenticationProviderPluginModule authProviderModule,String uri, TaskResult taskResult)
    {
        log.debug("Attempting full " + this + " on " + uri);
        final ApplicationLinkRequestFactory requestFactory = applicationLink
                .createAuthenticatedRequestFactory(authProviderModule.getAuthenticationProviderClass());
        return performRequest(uri, requestFactory, taskResult);
    }

    protected String performAuthenticatedRequest(ApplicationLink applicationLink, String uri, TaskResult taskResult)
    {
        log.debug("Attempting Authenticated request to on " + uri);
        final ApplicationLinkRequestFactory requestFactory = applicationLink
                .createAuthenticatedRequestFactory();
        return performRequest(uri, requestFactory, taskResult);
    }

    private String performRequest(String uri, ApplicationLinkRequestFactory requestFactory, TaskResult taskResult) {
        try
        {
            final ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, uri).addHeader("Content-Type", "application/json");
            return request.execute();
        }
        catch (ResponseException ex)
        {
            log.error(this + " failed: " + ex.getMessage());
            taskResult.fail(ex.getMessage());
            return null;
        }
        catch (CredentialsRequiredException ex)
        {
            log.error(this + " failed: " + ex.getMessage());
            taskResult.warning(ex.getMessage());
            //taskResult.addAction(authenticate());
            return null;
        }
    }

    @Override
    public boolean diagnose(ApplicationLink applicationLink, DiagnosticMessages diagnosticMessages, Map<String, String> testParameters) {
        return false;
    }
    public ArrayList<String> getTargetApplicationTypes()
    {
        // I know this is shit but...
        return Lists.newArrayList(
                new BambooApplicationTypeImpl(null, null).getI18nKey(),
                new ConfluenceApplicationTypeImpl(null, null).getI18nKey(),
                new FishEyeCrucibleApplicationTypeImpl(null, null).getI18nKey(),
                new GenericApplicationTypeImpl(null, null).getI18nKey(),
                new JiraApplicationTypeImpl(null, null).getI18nKey(),
                new RefAppApplicationTypeImpl(null, null).getI18nKey(),
                new StashApplicationTypeImpl(null, null).getI18nKey()
        );
    }

    @Override
    public void setScope(final String scope) {
        this.scope = scope;
    }
    public String getScope() {
        return this.scope;
    }

    protected void addKnowledgeBaseLink(TaskResult taskResult) {
        taskResult.addAction(TaskAction.knowledgeBase("https://confluence.atlassian.com/x/" + this.getURL(), this.getName()));
    }
}
