package com.atlassian.applinks.diagnostics.rest;

import javax.xml.bind.annotation.XmlElement;

/**
 * TODO: Document this class / interface here
 *
 * @since vX.X.X
 */
public class TestEntity extends LinkedEntity {
    @XmlElement (name = "className")
    private String className;

    @XmlElement (name = "name")
    private String name;

    @XmlElement (name = "scope")
    private String scope;

    @XmlElement (name = "applicationTypes")
    private String applicationTypes;

    public TestEntity()
    {
    }

    public TestEntity(final String name, final String className, final String scope, final String applicationTypes) {
        this.name = name;
        this.className = className;
        this.scope = scope;
        this.applicationTypes = applicationTypes;
    }
}
