package com.atlassian.applinks.diagnostics.rest;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.oauth.Consumer;
import com.google.common.base.Predicate;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @since ShipIt23
 */
public interface ApplicationLinkConfigurationService {
    List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink, Predicate<AuthenticationProviderPluginModule> predicate)
            throws URISyntaxException;

    List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink)
                    throws URISyntaxException;

    List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink, Iterable<AuthenticationProviderPluginModule> pluginModules)
                            throws URISyntaxException;

    ApplicationLink findApplicationLink(String id) throws TypeNotInstalledException;

    Iterable<Consumer> findConsumers(ApplicationLink applicationLink, List<AuthenticationProviderEntity> configuredAuthProviders);

    List<AuthenticationProviderPluginModule> getConfiguredProviderPluginModules(ApplicationLink applicationLink, Predicate<AuthenticationProviderPluginModule> predicate);

    boolean isConfigured(ApplicationLink applicationLink, AuthenticationProviderPluginModule authenticationProviderPluginModule);
}
