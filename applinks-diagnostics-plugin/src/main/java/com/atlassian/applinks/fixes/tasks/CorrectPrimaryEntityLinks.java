package com.atlassian.applinks.fixes.tasks;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

import com.atlassian.applinks.Utils.EntityLinkUtils;
import com.atlassian.applinks.core.property.PropertyService;
import com.atlassian.applinks.diagnostics.tasks.EntityLinkConsistencyTaskWorker;
import com.atlassian.applinks.fixes.AppLinkFixTask;
import com.atlassian.applinks.fixes.Audit;
import com.atlassian.applinks.host.spi.EntityReference;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import com.google.common.collect.Lists;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO: Document this class / interface here
 *
 * @since vX.X.X
 */
public class CorrectPrimaryEntityLinks implements AppLinkFixTask
{
    private static final Logger log = LoggerFactory.getLogger(CorrectPrimaryEntityLinks.class);

    private final List<String> linkedEntitiesWithInvalidPrimary;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final InternalHostApplication internalHostApplication;
    private final TypeAccessor typeAccessor;
    private final PropertyService propertyService;

    public CorrectPrimaryEntityLinks(final List<String> linkedEntitiesWithInvalidPrimary,
            final PluginSettingsFactory pluginSettingsFactory,
            final InternalHostApplication internalHostApplication,
            final TypeAccessor typeAccessor,
            final PropertyService propertyService)
    {
        this.linkedEntitiesWithInvalidPrimary = linkedEntitiesWithInvalidPrimary;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.internalHostApplication = internalHostApplication;
        this.typeAccessor = typeAccessor;
        this.propertyService = propertyService;
    }

    @Override
    public String getName()
    {
        return "Corrent Primary Entity Links";
    }

    @Override
    public List<Audit> fix()
    {
        List<Audit> auditTrail = Lists.newArrayList();
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        // list of entities...
        for(EntityReference entityReference : this.internalHostApplication.getLocalEntities())
        {
            final String localEntity = getLocalEntityReferenceKey(entityReference);
            if(!linkedEntitiesWithInvalidPrimary.contains(localEntity))
            {
                continue;
            }

            List<String> encodedLinks = EntityLinkUtils.getEncodedLinks(typeAccessor, pluginSettings, entityReference);
            if (encodedLinks == null || encodedLinks.size() == 0)
            {
                continue;
            }

            List<String> fixedPrimaryForType = Lists.newArrayList();

            // for each entity link
            for(String encodedLinkString : encodedLinks)
            {
                final JSONObject encodedLink = EntityLinkConsistencyTaskWorker.getEncodedLinkJsonObject(encodedLinkString);

                String type = EntityLinkUtils.getJSONString(encodedLink, "type");
                final String primaryLinkKey = EntityLinkConsistencyTaskWorker.getPrimaryLinkKey(typeAccessor, entityReference, type);

                // set it to be the primary, simplistic and it means the final entry of each type will become the primary but its foolproof?
                final Properties primary = new Properties();
                primary.put("key", EntityLinkUtils.getJSONString(encodedLink, "key"));
                primary.put("applicationId", EntityLinkUtils.getJSONString(encodedLink, "applicationId"));

                // Audit
                StringWriter writer = new StringWriter();
                primary.list(new PrintWriter(writer));
                Audit audit = new Audit(primaryLinkKey, "null", writer.getBuffer().toString());
                auditTrail.add(audit);

                pluginSettings.put(primaryLinkKey, primary);
            }
        }
        return auditTrail;
    }

    public static String getLocalEntityReferenceKey(EntityReference entityReference)
    {
        return entityReference.getType().getI18nKey() + ":" + entityReference.getKey() + "/" + entityReference.getName();
    }
}
