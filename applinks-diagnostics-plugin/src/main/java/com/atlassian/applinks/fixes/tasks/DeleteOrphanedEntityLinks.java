package com.atlassian.applinks.fixes.tasks;

import java.util.List;

import com.atlassian.applinks.Utils.EntityLinkUtils;
import com.atlassian.applinks.fixes.AppLinkFixTask;
import com.atlassian.applinks.fixes.Audit;
import com.atlassian.applinks.fixes.rest.FixApplicationLinkResource;
import com.atlassian.applinks.host.spi.EntityReference;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import com.google.common.collect.Lists;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Delete orphaned entity links
 *
 * @since v4.0.0
 */
public class DeleteOrphanedEntityLinks implements AppLinkFixTask
{
    private static final Logger log = LoggerFactory.getLogger(FixApplicationLinkResource.class);

    private final PluginSettingsFactory pluginSettingsFactory;
    private final InternalHostApplication internalHostApplication;
    private final TypeAccessor typeAccessor;
    private final List<String> missingParentIds;

    public DeleteOrphanedEntityLinks(final List<String> missingParentIds,
            final PluginSettingsFactory pluginSettingsFactory,
            final InternalHostApplication internalHostApplication,
            final TypeAccessor typeAccessor)
    {
        this.missingParentIds = missingParentIds;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.internalHostApplication = internalHostApplication;
        this.typeAccessor = typeAccessor;
    }

    @Override
    public String getName()
    {
        return "Delete Orphaned Entity Links";
    }

    @Override
    public List<Audit> fix()
    {
        List<Audit> auditTrail = Lists.newArrayList();

        log.debug("Attempting to delete entitylinks refering to missing parent ids [" + StringUtils.join(missingParentIds.toArray()) + "]");
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();

        // list of entities...
        for(EntityReference entityReference : this.internalHostApplication.getLocalEntities())
        {
            List<String> encodedLinks = EntityLinkUtils.getEncodedLinks(typeAccessor, pluginSettings, entityReference);
            if (encodedLinks == null)
            {
                continue;
            }

            // create the fix
            boolean applyFix = false;
            List<String> fixedLinks = Lists.newArrayList();
            for (final String linkDefinition : encodedLinks)
            {
                for(String parentId : missingParentIds)
                {
                    // remove any link referring to the missing parent id.
                    if(!linkDefinition.contains(parentId))
                    {
                        fixedLinks.add(linkDefinition);
                    }
                    else
                    {
                        applyFix = true;
                    }
                }

            }

            if(applyFix)
            {
                Audit audit = new Audit(entityReference.getType() + ":" + entityReference.getKey(), StringUtils.join(encodedLinks.toArray()), StringUtils.join(fixedLinks.toArray()));
                auditTrail.add(audit);
                // apply the fix
                EntityLinkUtils.setEncodedLinks(typeAccessor, pluginSettings, entityReference, fixedLinks);
            }
        }
        return auditTrail;
    }
}
