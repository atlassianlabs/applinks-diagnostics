package com.atlassian.applinks.fixes;

import javax.xml.bind.annotation.XmlAttribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @since v4.0.0
 */
public class Audit
{
    private static final Logger log = LoggerFactory.getLogger(Audit.class);
    private String item;
    private String originalValue;
    private String fixedValue;

    public Audit(String item, String originalValue, String fixedValue)
    {

        this.item = item;
        this.originalValue = originalValue;
        this.fixedValue = fixedValue;
        log.info(String.format("Applinks Diagnostics Audit: item [%s] originalValue [%s] fixedValue [%s]", item, originalValue, fixedValue));
    }

    @XmlAttribute
    public String getItem()
    {
        return item;
    }

    @XmlAttribute
    public String getOriginalValue()
    {
        return originalValue;
    }

    @XmlAttribute
    public String getFixedValue()
    {
        return fixedValue;
    }
}
