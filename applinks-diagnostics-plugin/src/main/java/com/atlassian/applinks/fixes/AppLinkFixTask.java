package com.atlassian.applinks.fixes;

import java.util.List;

public interface AppLinkFixTask
{
    /**
     * Get the task name.
     * @return
     */
    String getName();

    List<Audit> fix();
}
