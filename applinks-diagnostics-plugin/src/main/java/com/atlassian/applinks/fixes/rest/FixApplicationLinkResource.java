package com.atlassian.applinks.fixes.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.rest.AbstractResource;
import com.atlassian.applinks.fixes.AppLinkFixService;
import com.atlassian.applinks.fixes.AppLinkFixTask;
import com.atlassian.applinks.fixes.Audit;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.net.RequestFactory;

import com.sun.jersey.spi.resource.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This rest end point is used when displaying application links.
 *
 * @since ShipIt23
 */
@Path ("fixApplicationLink")
@Consumes ({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Singleton
//@InterceptorChain ({ ContextInterceptor.class, AdminApplicationLinksInterceptor.class })
@ExperimentalApi
public class FixApplicationLinkResource extends AbstractResource
{
    private static final Logger log = LoggerFactory.getLogger(FixApplicationLinkResource.class);

    private final AppLinkFixService appLinkFixService;

    public FixApplicationLinkResource
            (
                    final MutatingApplicationLinkService applicationLinkService,
                    final RestUrlBuilder restUrlBuilder,
                    final RequestFactory requestFactory,
                    final InternalTypeAccessor typeAccessor,
                    final AppLinkFixService appLinkFixService)
    {
        super (restUrlBuilder, typeAccessor, requestFactory, applicationLinkService);
        this.appLinkFixService = appLinkFixService;
    }

    @POST
    @Path("{fixId}")
    public Response fixApplicationLink(@PathParam("fixId") final String fixId)
    {
        log.debug("Run fix [" + fixId + "]");
        final AppLinkFixTask fixTask = appLinkFixService.getFixTask(fixId);
        if(fixTask == null)
        {
            log.error("No fix found for [" + fixId + "]. The test may already have been run?");
            return Response.noContent().build();
        }

        List<Audit> auditTrail = fixTask.fix ();
        return Response.ok().entity(auditTrail).build();

    }
}