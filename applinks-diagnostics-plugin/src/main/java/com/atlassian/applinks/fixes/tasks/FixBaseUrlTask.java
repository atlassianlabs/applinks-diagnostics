package com.atlassian.applinks.fixes.tasks;

import java.util.List;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.fixes.AppLinkFixTask;
import com.atlassian.applinks.fixes.Audit;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;

import com.google.common.collect.Lists;

/**
 * @since ShipIt23
 */
public class FixBaseUrlTask implements AppLinkFixTask
{
    private final ManifestRetriever manifestRetriever;
    private final ApplicationId applicationLinkId;
    private final MutatingApplicationLinkService mutatingApplicationLinkService;

    public FixBaseUrlTask(ApplicationId applicationLinkId, MutatingApplicationLinkService mutatingApplicationLinkService,  ManifestRetriever manifestRetriever)
    {
        this.applicationLinkId = applicationLinkId;
        this.mutatingApplicationLinkService = mutatingApplicationLinkService;

        this.manifestRetriever = manifestRetriever;
    }
    public String getName()
    {
        return "Fix BASE URL";
    }

    public List<Audit> fix()
    {
        List<Audit> auditTrail = Lists.newArrayList();
        try
        {
            MutableApplicationLink applicationLink = mutatingApplicationLinkService.getApplicationLink(applicationLinkId);
            Manifest manifest = manifestRetriever.getManifest(applicationLink.getRpcUrl());
            applicationLink.update(ApplicationLinkDetails.builder(applicationLink).rpcUrl(manifest.getUrl()).build());
        }
        catch (ManifestNotFoundException e)
        {
            return auditTrail;
        }
        catch (TypeNotInstalledException e)
        {
            return auditTrail;
        }
        return auditTrail;
    }
}
