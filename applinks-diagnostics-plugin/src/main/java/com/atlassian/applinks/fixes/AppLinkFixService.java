package com.atlassian.applinks.fixes;

import java.util.HashMap;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppLinkFixService
{
    private static final Logger LOG = LoggerFactory.getLogger(AppLinkFixService.class);

    // TODO some kind of intelligence to remove stale items.
    private final HashMap<UUID,AppLinkFixTask> cache = new HashMap<UUID, AppLinkFixTask>();

    public AppLinkFixService()
    {
    }

    public AppLinkFixTask getFixTask(String fixId)
    {
        AppLinkFixTask task = cache.get(UUID.fromString(fixId));
        if(task != null)
        {
            cache.remove(fixId);
        }
        return task;
    }

    public UUID addFixTask(AppLinkFixTask fix)
    {
        UUID uuid = UUID.randomUUID();
        cache.put(uuid, fix);
        return uuid;
    }
}
