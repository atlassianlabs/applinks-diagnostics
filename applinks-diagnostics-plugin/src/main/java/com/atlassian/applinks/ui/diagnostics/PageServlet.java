package com.atlassian.applinks.ui.diagnostics;

import com.atlassian.applinks.servlet.AbstractSecureServlet;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;

public class PageServlet extends AbstractSecureServlet
{
    private final static String TEMPLATE = "vm/applinks-diagnostics-page.vm";
    private final TemplateRenderer templateRenderer;
    private final I18nResolver i18nResolver;
    private final WebResourceManager webResourceManager;
    private UserManager userManager;

    public PageServlet(final TemplateRenderer templateRenderer, final I18nResolver i18nResolver, final WebResourceManager webResourceManager,
                       final UserManager userManager,
                       final LoginUriProvider loginUriProvider) {
        super(userManager, loginUriProvider);
        this.templateRenderer = templateRenderer;
        this.i18nResolver = i18nResolver;
        this.webResourceManager = webResourceManager;
        this.userManager = userManager;
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        // check authorization
        super.doGet(request, response);
        if (response.isCommitted()) return;

        UserProfile user = userManager.getRemoteUser(request);
        if (user == null || !userManager.isSystemAdmin(user.getUserKey()))
        {
            response.sendError(401);
            return;
        }

        webResourceManager.requireResourcesForContext("applinks-diagnostics");
        response.setContentType("text/html; charset=utf-8");
        templateRenderer.render(TEMPLATE, response.getWriter());
    }
}
