AJS.test.require("com.atlassian.applinks.applinks-diagnostics-plugin:applinks-diagnostics-resources-test", function() {

    var Test = AppLinksDiagnostics.Entities.Test;

    module("AppLinksDiagnostics.Entities.Test");

    test("It can construct a model with the basic info", function() {
        var json = {
            "name": "RPC Url HTTP Connectivity Test",
            "details": "Testing HTTP connectivity to [http://localhost:8090] for [Confluence]"
        }

        var model = new Test(json, {parse: true});
        equal(model.get("name"), "RPC Url HTTP Connectivity Test", "It captures the name");
        equal(model.get("details"), "Testing HTTP connectivity to [http://localhost:8090] for [Confluence]", "It captures the name");
    })

    test("It can construct a PASS model", function() {
        var json = {
            "statusMessages": [,
                {
                    "status": "INFO",
                    "text": "POST to <url>"
                },
                {
                    "status": "PASS",
                    "text": "Successfully pinged to Confluence"
                }
            ]
        }

        var model = new Test(json, {parse: true});
        equal(model.get("status"), "pass");
    })

    test("It can construct a WARNING model", function() {
        var json = {
            "statusMessages": [,
                {
                    "status": "INFO",
                    "text": "POST to <url>"
                },
                {
                    "status": "WARNING",
                    "text": "Successfully pinged to Confluence"
                }
            ]
        }

        var model = new Test(json, {parse: true});
        equal(model.get("status"), "warn");
    })

    test("It can construct a FAIL model", function() {
        var json = {
            "statusMessages": [,
                {
                    "status": "INFO",
                    "text": "POST to <url>"
                },
                {
                    "status": "FAIL",
                    "text": "Successfully pinged to Confluence"
                }
            ]
        }

        var model = new Test(json, {parse: true});
        equal(model.get("status"), "fail");
    })

    test("It can extract the info for a model", function() {
        var json = {
            "statusMessages": [,
                {
                    "status": "INFO",
                    "text": "POST to <url>"
                },
                {
                    "status": "INFO",
                    "text": "< 200 Ok"
                },
                {
                    "status": "FAIL",
                    "text": "Successfully pinged to Confluence"
                }
            ]
        }

        var model = new Test(json, {parse: true});
        equal(model.get("info"), "POST to <url>\n< 200 Ok");
    })

    test("It can extract the KB information", function() {
        var json = {
            "actions": [{
                "method": "POST",
                "actionUrl": "https://extranet.atlassian.com/display/AR/AppLinks+Diagnostics+-+RPC+Url+HTTP+Connectivity+Test",
                "description": "RPC Url HTTP Connectivity Test",
                "actionType": "KNOWLEDGE_BASE"
            }]
        }

        var model = new Test(json, {parse: true});
        equal(model.get("kbURL"), "https://extranet.atlassian.com/display/AR/AppLinks+Diagnostics+-+RPC+Url+HTTP+Connectivity+Test", "It extracts the KB URL");
        equal(model.get("kbName"), "RPC Url HTTP Connectivity Test", "It extracts the KB name");
    })
})
