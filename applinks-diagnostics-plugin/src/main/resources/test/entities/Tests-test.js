AJS.test.require("com.atlassian.applinks.applinks-diagnostics-plugin:applinks-diagnostics-resources-test", function() {

    var Tests = AppLinksDiagnostics.Entities.Tests;
    var Test = AppLinksDiagnostics.Entities.Test;

    module("AppLinksDiagnostics.Entities.Tests");

    test("It can extract the collection from the JSON response", function() {
        var json = {
            "taskResults": [
                {
                    "name": "RPC Url HTTP Connectivity Test",
                    "details": "Testing HTTP connectivity to [http://localhost:8090] for [Confluence]"
                },
                {
                    "name": "Display Url HTTP Connectivity Test",
                    "details": "Testing HTTP connectivity to [http://localhost:8090] for [Confluence]"
                }
            ]
        };

        var collection = new Tests(json, {parse: true});

        equal(collection.length, 2, "Captures the models in the response");
        equal(collection.models[0].get("name"), "RPC Url HTTP Connectivity Test", "The name of the first model");
        equal(collection.models[1].get("name"), "Display Url HTTP Connectivity Test", "The name of the second model");
    })

    test("It detects that all the models are passing", function() {
        var collection = new Tests([
            new Test({"status": "pass"}),
            new Test({"status": "pass"})
        ]);

        ok(collection.isPass());
    })

    test("It detects that not all the models are passing", function() {
        var collection = new Tests([
            new Test({"status": "pass"}),
            new Test({"status": "warn"})
        ]);

        ok(!collection.isPass());
    })

    test("It can extract all the tests", function() {
        var passModel = new Test({"status": "pass"});
        var failModel = new Test({"status": "fail"});
        var warnModel = new Test({"status": "warn"});
        var collection = new Tests([passModel, failModel, warnModel]);

        var json =[
            {
                "status": "pass"
            },
            {
                "status": "fail"
            },
            {
                "status": "warn"
            }
        ];
        deepEqual(collection.getAllTests(), json);
    })

    test("It can extract the fail & warn models", function() {
        var passModel = new Test({"status": "pass"});
        var failModel = new Test({"status": "fail"});
        var warnModel = new Test({"status": "warn"});
        var collection = new Tests([passModel, failModel, warnModel]);

        var json =[
            {
                "status": "fail"
            },
            {
                "status": "warn"
            }
        ];
        deepEqual(collection.getNonPassTests(), json);
    })


});
