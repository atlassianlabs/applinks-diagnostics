AJS.test.require("com.atlassian.applinks.applinks-diagnostics-plugin:applinks-diagnostics-resources-test", function() {

    var AppLinks = AppLinksDiagnostics.Entities.AppLinks;

    module("AppLinksDiagnostics.Entities.AppLinks");

    test("It calls the REST endpoint to get the list of applinks", function() {
        var server = this.sandbox.useFakeServer();
        var collection = new AppLinks();

        collection.fetch();

        equal(server.requests.length, 1, "Sends a request to the backend");
        ok(server.requests[0].url.match(AJS.contextPath() + "/rest/applinksdiagnostics/1.0/abinitioApplicationLink/applicationLink/list.json"), "Sends a request to the right REST resource");
    });
});
