AJS.test.require("com.atlassian.applinks.applinks-diagnostics-plugin:applinks-diagnostics-resources-test", function() {

    var AppLink = AppLinksDiagnostics.Views.AppLink;
    var Backbone = AppLinksDiagnostics.Libs.Backbone;

    module("AppLinksDiagnostics.Views.AppLink");

    test("It renders a row for the applinks table", function() {
        var view = new AppLink({
            model: new Backbone.Model()
        });
        view.render();

        ok(view.$el.is("tr"), "Renders a row");
        equal(view.$el.find("td").length, 6, "Renders 6 cells");
    })

    test("It renders a the applink icon in the first cell", function() {
        var view = new AppLink({
            model: new Backbone.Model({
                iconUrl: 'theIconUrl'
            })
        });
        view.render();

        var cell = view.$el.find("td").eq(0);
        equal(cell.find("img").length, 1, "Renders an img");
        equal(cell.find("img").attr("src"), "theIconUrl", "Uses 'iconURL' as the url");
    })

    test("It renders a the name in the second cell", function() {
        var view = new AppLink({
            model: new Backbone.Model({
                name: 'theName'
            })
        });
        view.render();

        var cell = view.$el.find("td").eq(1);
        equal(cell.text(), "theName", "Uses 'name' as the name");
    })

    test("It renders a link to the display URL in the third cell", function() {
        var view = new AppLink({
            model: new Backbone.Model({
                displayUrl: 'theDisplayURL'
            })
        });
        view.render();

        var cell = view.$el.find("td").eq(2);
        equal(cell.find("a").length, 1, "Renders a link");
        equal(cell.find("a").attr("href"), "theDisplayURL", "Uses 'displayUrl' as the url");
        equal(cell.find("a").text(), "theDisplayURL", "Uses 'displayUrl' as the link's text");
        equal(cell.find("a").attr("target"), "_blank", "The link will open in a new page");
    })

    test("It renders a link to the RPC URL in the fourth cell", function() {
        var view = new AppLink({
            model: new Backbone.Model({
                rpcUrl: 'theRpcUrl'
            })
        });
        view.render();

        var cell = view.$el.find("td").eq(3);
        equal(cell.find("a").length, 1, "Renders a link");
        equal(cell.find("a").attr("href"), "theRpcUrl", "Uses 'rpcUrl' as the url");
        equal(cell.find("a").text(), "theRpcUrl", "Uses 'rpcUrl' as the link's text");
        equal(cell.find("a").attr("target"), "_blank", "The link will open in a new page");
    })

    test("It renders a the type in the fifth cell", function() {
        var view = new AppLink({
            model: new Backbone.Model({
                type: 'theType'
            })
        });
        view.render();

        var cell = view.$el.find("td").eq(4);
        equal(cell.text(), "theType", "Uses 'type' as the type");
    })

    test("It renders a link to run the test in the sixth cell", function() {
        var view = new AppLink({
            model: new Backbone.Model()
        });
        view.render();

        var cell = view.$el.find("td").eq(5);
        equal(cell.find("a").length, 1, "Renders a link");
    })

    test("When clicking on the test link, it triggers the 'test' event with the model", function() {
        var onTest = sinon.spy();
        var model = new Backbone.Model({
            applicationId: "1234-5678"
        });
        var view = new AppLink({
            model: model
        });
        view.render();
        view.on("test", onTest);

        var link = view.$el.find("td").eq(5).find("a");
        link.click();

        sinon.assert.calledOnce(onTest);
        sinon.assert.calledWithExactly(onTest, model);
    })
});
