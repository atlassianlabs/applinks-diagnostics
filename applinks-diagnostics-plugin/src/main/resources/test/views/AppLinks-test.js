AJS.test.require("com.atlassian.applinks.applinks-diagnostics-plugin:applinks-diagnostics-resources-test", function() {

    var AppLinksView = AppLinksDiagnostics.Views.AppLinks;
    var AppLinkView = AppLinksDiagnostics.Views.AppLink;
    var AppLinksCollection = AppLinksDiagnostics.Entities.AppLinks;
    var Backbone = AppLinksDiagnostics.Libs.Backbone;

    module("AppLinksDiagnostics.Views.AppLinks");

    test("It renders an AUI table", function() {
        var view = new AppLinksView({
            collection: new AppLinksCollection()
        });
        view.render();

        ok(view.$el.find("table.aui"), "Renders a table");
    })

    test("It renders a ItemView for each applink", function() {
        var appLink1 = new Backbone.Model();
        var appLink2 = new Backbone.Model();
        var view = new AppLinksView({
            collection: new AppLinksCollection([appLink1, appLink2])
        });
        view.render();

        ok(appLink1.view instanceof AppLinkView, "Model1 uses an AppLinksView");
        ok(appLink2.view instanceof AppLinkView, "Model2 uses an AppLinksView");
    })

    test("It rethrows the 'test' event form an ItemView", function() {
        var onTest = this.spy();
        var appLink = new Backbone.Model();
        var view = new AppLinksView({
            collection: new AppLinksCollection([appLink])
        });
        view.render();
        view.on('test', onTest);

        appLink.view.trigger("test", "1234-5678");

        sinon.assert.calledOnce(onTest);
        sinon.assert.calledWithExactly(onTest, "1234-5678");
    })    
});
