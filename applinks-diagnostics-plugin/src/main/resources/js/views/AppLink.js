AppLinksDiagnostics = window.AppLinksDiagnostics || {};
AppLinksDiagnostics.Views = AppLinksDiagnostics.Views || {};
AppLinksDiagnostics.Views.AppLink = (function(
    Backbone,
    TableItemTemplate
){
    return Backbone.View.extend({
        template: TableItemTemplate,

        render: function() {
            this.setElement(this.template(this.model.attributes));
            return this;
        },

        events: {
            "click .test-applink": function(ev) {
                this.trigger('test', this.model);
                ev.preventDefault();
            }
        },

        startSpinner: function(){
            this.$(".button-spinner").spin();
        },

        stopSpinner: function() {
            this.$(".button-spinner").spinStop();
        }
    });
}(
    AppLinksDiagnostics.Libs.Backbone,
    AppLinksDiagnostics.Templates.TableItem
));
