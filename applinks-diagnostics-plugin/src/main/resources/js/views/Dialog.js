AppLinksDiagnostics = window.AppLinksDiagnostics || {};
AppLinksDiagnostics.Views = AppLinksDiagnostics.Views || {};
AppLinksDiagnostics.Views.Dialog = (function(
    Backbone,
    DialogTemplate
){
    return Backbone.View.extend({
        template: DialogTemplate,

        initialize: function(options) {
            this.appLinkModel = options.appLink;
            this.testsModel = options.tests;
        },

        serializeData: function() {
            return {
                applicationName: this.appLinkModel.get("name"),
                isPass: this.testsModel.isPass(),
                nonPassResults: this.testsModel.getNonPassTests(),
                results: this.testsModel.getAllTests()
            }
        },

        render: function() {
            this.setElement(this.template(this.serializeData()));
            this.el.id = this.id;
            return this;
        },

        _showAllResults: function() {
            this.$("#total-results").attr("aria-hidden", "false");
            this.$("#dialog-test-view-all").attr("aria-pressed", "true");
        },

        _hideAllResults: function() {
            this.$("#total-results").attr("aria-hidden", "true");
            this.$("#dialog-test-view-all").attr("aria-pressed", "false");
        },

        _showPartialResults: function() {
            this.$("#partial-results").attr("aria-hidden", "false");
            this.$("#dialog-test-view-partial").attr("aria-pressed", "true");
        },

        _hidePartialResults: function() {
            this.$("#partial-results").attr("aria-hidden", "true");
            this.$("#dialog-test-view-partial").attr("aria-pressed", "false");
        },

        _showDiagnosticsResults: function() {
            this.$("#diagnostics-results").attr("aria-hidden", "false");
            this.$("#dialog-test-view-diagnostics").attr("aria-pressed", "true");
        },

        _hideDiagnosticsResults: function() {
            this.$("#diagnostics-results").attr("aria-hidden", "true");
            this.$("#dialog-test-view-diagnostics").attr("aria-pressed", "false");
        },

        events: {
            'click #dialog-test-view-all': function() {
                this._showAllResults();
                this._hidePartialResults();
                this._hideDiagnosticsResults();
            },
            'click #dialog-test-view-partial': function() {
                this._hideAllResults();
                this._showPartialResults();
                this._hideDiagnosticsResults();
            },
            'click #dialog-test-view-diagnostics': function() {
                this._hideAllResults();
                this._hidePartialResults();
                this._showDiagnosticsResults();
            },
            'click #dialog-test-close': function(e) {
                e.preventDefault();
                this.trigger("close");
            }
        }
    });
}(
    AppLinksDiagnostics.Libs.Backbone,
    AppLinksDiagnostics.Templates.Dialog
));
