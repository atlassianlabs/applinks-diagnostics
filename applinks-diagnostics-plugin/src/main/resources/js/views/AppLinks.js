AppLinksDiagnostics = window.AppLinksDiagnostics || {};
AppLinksDiagnostics.Views = AppLinksDiagnostics.Views || {};
AppLinksDiagnostics.Views.AppLinks = (function(
    Backbone,
    TableTemplate,
    AJS,
    AppLink
){
    return Backbone.View.extend({
        template: TableTemplate,

        render: function() {
            var table = AJS.$(this.template());
            var tbody = AJS.$(table).find("tbody");

            for (var i=0; i<this.collection.models.length; i++) {
                var model = this.collection.models[i];
                var modelView = new AppLink({
                    model: model
                });
                this.listenTo(modelView, "test", function(model) {
                    this.trigger("test", model);
                });
                tbody.append(modelView.render().$el);
                model.view = modelView;
            }

            this.$el.empty().append(table);
        },

        startSpinner: function(applinkId) {
            var model = this.collection.findWhere({applicationId: applinkId});
            if (model) {
                model.view.startSpinner();
            }
        },

        stopSpinner: function(applinkId) {
            var model = this.collection.findWhere({applicationId: applinkId});
            if (model) {
                model.view.stopSpinner();
            }
        },

        stopAllSpinners: function() {
            this.collection.each(function(model) {
                if (model.view) {
                    model.view.stopSpinner();
                }
            });
        }
    });
}(
    AppLinksDiagnostics.Libs.Backbone,
    AppLinksDiagnostics.Templates.Table,
    AJS,
    AppLinksDiagnostics.Views.AppLink
));
