AppLinksDiagnostics = window.AppLinksDiagnostics || {};

AppLinksDiagnostics.Libs = {
    Underscore: _.noConflict(),
    Backbone: Backbone.noConflict()
};
