AppLinksDiagnostics = window.AppLinksDiagnostics || {};
AppLinksDiagnostics.init = (function(
    AppLinksEntity,
    AppLinksTable,
    Dialog
){
    return function(container) {
        var appLinks = new AppLinksEntity();
        appLinks.on("sync", function() {
            tableView.render();
        });

        var tableView = new AppLinksTable({
            collection: appLinks
        });
        tableView.setElement(container);
        tableView.on("test", function(appLinkModel) {
            Dialog.show(appLinkModel);
            tableView.stopAllSpinners();
            tableView.startSpinner(appLinkModel.get("applicationId"));
        });

        Dialog.on("render", function(applicationId) {
            AppLinks.SPI.addAuthenticationTrigger("#authenticate-link", AJS.contextPath() + "/plugins/servlet/applinks/oauth/login-dance/authorize?applicationLinkID=" + applicationId, {
                onSuccess: function() {
                    AJS.$("#dialog-test-close").click();
                    tableView.trigger("test", tableView.collection.findWhere({applicationId: applicationId}));
                }
            });
            tableView.stopSpinner(applicationId);
        });

        appLinks.fetch();

        // Monkey patch goog utils, used by FECRU.
        if (goog && goog.string && !goog.string.newLineToBr) {
            goog.string.newLineToBr = goog.string.newlineToBr;
            goog.string.NEWLINE_TO_BR_RE_ = goog.string.NEWLINE_TO_BR_RE;
        }
    }
}(
    AppLinksDiagnostics.Entities.AppLinks,
    AppLinksDiagnostics.Views.AppLinks,
    AppLinksDiagnostics.Modules.Dialog
));
