AppLinksDiagnostics = window.AppLinksDiagnostics || {};
AppLinksDiagnostics.Modules = AppLinksDiagnostics.Modules || {};
AppLinksDiagnostics.Modules.Dialog = (function(
    DialogView,
    TestsEntity,
    Backbone,
    _,
    AJS
){
    var dialog;

    return _.extend(Backbone.Events, {
        show: function(appLinkModel) {
            if (dialog) {
                this.hide();
            }

            var applicationId = appLinkModel.get("applicationId");
            var dialogId = "applink-dialog-"+ applicationId;
            var testsModel = new TestsEntity(applicationId);

            this.listenTo(testsModel, "sync", function() {
                dialog = new DialogView({
                    appLink: appLinkModel,
                    tests: testsModel,
                    id: dialogId
                });
                dialog.render();

                var popup = AJS.popup({
                    width: 980,
                    height: 440
                });
                popup.element.append(dialog.$el);

                this.listenTo(dialog, 'close', function() {
                    popup.hide();
                });

                AJS.$(document).on("hideLayer", _.bind(function(ev,type,evPopup) {
                    if (evPopup == popup) {
                        this.hide();
                        popup.element.detach();
                        popup = null;
                    }
                }, this));

                popup.show();

                this.trigger("render", applicationId);

            }, this);

            testsModel.fetch();
        },

        hide: function() {
            this.stopListening();
            dialog.$el.detach();
            dialog = null;
        }
    })
}(
    AppLinksDiagnostics.Views.Dialog,
    AppLinksDiagnostics.Entities.Tests,
    AppLinksDiagnostics.Libs.Backbone,
    AppLinksDiagnostics.Libs.Underscore,
    AJS
));