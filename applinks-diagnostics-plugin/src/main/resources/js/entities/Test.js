AppLinksDiagnostics = window.AppLinksDiagnostics || {};
AppLinksDiagnostics.Entities = AppLinksDiagnostics.Entities || {};
AppLinksDiagnostics.Entities.Test = (function(
    Backbone
){
    return Backbone.Model.extend({
        parse: function(data, response) {
            var results = {};

            // Capture basic info
            results.name = data.name;
            results.details = data.details;
            results.installRequired = false;

            // Extract the first KB, Authenticate and Install links
            for (var i = 0; i < data.actions.length; i++) {
                if (!results.installRequired && data.actions[i].actionType == "INSTALL") {
                    results.installRequired = true;
                    results.status = "warn";
                } else if (!results.authUri && data.actions[i].actionType == "AUTHENTICATE") {
                    results.authUri = data.actions[i].authUri;
                    results.status = "warn";
                } else if (!results.kbURL && data.actions[i].actionType == "KNOWLEDGE_BASE") {
                    results.kbURL = data.actions[i].actionUrl;
                    results.kbName = data.actions[i].description;
                }
            }

            // Locate status text (for results table)
            if (!results.authUri && !results.installRequired && _.isArray(data.statusMessages) && data.statusMessages.length) {
                if (_.some(data.statusMessages, function(message) { return message.status == "PASS"}) &&
                        _.every(data.statusMessages, function(message) { return message.status == "PASS" || message.status == "INFO"})) {
                    // Pass when at least one PASS exists, and no messages other than PASS and INFO
                    results.status = "pass";
                    // Pick the most recent PASS message for status text
                    results.statusText = _.last(_.filter(data.statusMessages, function(message) { return message.status == "PASS"})).text;
                } else {
                    // Pick the most recent FAIL if there is one
                    var status;
                    status = _.last(_.filter(data.statusMessages, function(message) { return message.status == "FAIL" }));
                    if (status) {
                        results.statusText = status.text;
                        results.status = "fail";
                    }
                    if (!results.statusText) {
                        // Fallback to pick the most recent WARNING if there is one
                        status = _.last(_.filter(data.statusMessages, function(message) { return message.status == "WARNING" }));
                        if (status) {
                            results.statusText = status.text;
                            results.status = "warn"
                        }
                    }
                    if (!results.statusText) {
                        // Fallback to just pick the last message, and treat it as a failure message
                        results.statusText = _.last(data.statusMessages).text;
                        results.status = "fail";
                    }
                }
            }

            // Capture log (for diagnostics tab)
            if (_.isArray(data.statusMessages) && data.statusMessages.length) {
                results.info = _.chain(data.statusMessages)
                    .pluck("text")
                    .value()
                    .join("\n")
            }

            // Capture timestamp
            if (response && response.xhr) {
                results.timestamp = response.xhr.getResponseHeader("Date");
            }

            return results;
        }
    });
}(
    AppLinksDiagnostics.Libs.Backbone
));
