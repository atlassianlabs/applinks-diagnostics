AppLinksDiagnostics = window.AppLinksDiagnostics || {};
AppLinksDiagnostics.Entities = AppLinksDiagnostics.Entities || {};
AppLinksDiagnostics.Entities.Tests = (function(
    Backbone,
    _,
    TestModel,
    AJS
){
    return Backbone.Collection.extend({
        initialize: function(applinkId) {
            this.applinkId=applinkId

        },
        url: function() {
            return AJS.contextPath() + "/rest/applinksdiagnostics/1.0/diagnoseApplicationLink/" + this.applinkId;
        },
        model: TestModel,

        parse: function(data) {
            return data.taskResults;
        },

        isPass: function() {
            return this.every(function(model) {
                return model.get("status") == "pass";
            })
        },

        getAllTests: function() {
            return this.invoke("toJSON")
        },

        getNonPassTests: function() {
            return _.invoke(this.filter(function(model) { return model.get("status") != "pass"}), "toJSON")
        }
    });
}(
    AppLinksDiagnostics.Libs.Backbone,
    AppLinksDiagnostics.Libs.Underscore,
    AppLinksDiagnostics.Entities.Test,
    AJS
));
