AppLinksDiagnostics = window.AppLinksDiagnostics || {};
AppLinksDiagnostics.Entities = AppLinksDiagnostics.Entities || {};
AppLinksDiagnostics.Entities.AppLinks = (function(
    Backbone,
    AJS
){
    var AppLinks = Backbone.Collection.extend({
        url: AJS.contextPath() + "/rest/applinksdiagnostics/1.0/abinitioApplicationLink/applicationLink/list.json"
    });

    return AppLinks;
}(
    AppLinksDiagnostics.Libs.Backbone,
    AJS
));