package it.com.atlassian.applinks.diagnostics.rest;

import java.io.IOException;
import java.util.UUID;

import com.atlassian.applinks.diagnostics.rest.DiagnoseApplicationLinkResource;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.core.net.HttpClientResponse;

import org.json.JSONException;
import org.junit.Test;

import it.com.atlassian.applinks.rest.RestHelper;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Michael Minns on 05/02/14.
 */
public class DiagnoseApplicationLinkResourceTest
{
    private static final String URL_REST_CONTEXT = "/rest";
    private static final String URL_DIAGNOSTICS_CONTEXT = "/applinksdiagnostics";
    private static final String URL_VERSION_CONTEXT = "/latest";
    private static final String URL_DIAGNOSTICS_RESOURCE_CONTEXT = URL_REST_CONTEXT + URL_DIAGNOSTICS_CONTEXT + URL_VERSION_CONTEXT + "/" + DiagnoseApplicationLinkResource.DIAGNOSE_APPLICATION_LINK_CONTEXT;
    private static final String REST_RESOURCE_URL_FORMAT = "%s" + URL_DIAGNOSTICS_RESOURCE_CONTEXT + "%s";

    private static final String TEST_INSTANCE_BASE_URL = System.getProperty("baseUrl", String.format("http://%s:%s/%s", System.getProperty("baseUrlHost", RestHelper.getLocalHostName()), System.getProperty("baseUrlPort", "5990"), System.getProperty("baseUrlContext", "refapp")));

    @Test
    public void verifyTestsResourceIsPresent() throws IOException, JSONException, ResponseException
    {
        String url = String.format(REST_RESOURCE_URL_FORMAT, TEST_INSTANCE_BASE_URL,  "/test/list.json");
        final HttpClientResponse response = RestHelper.getRestResponse(RestHelper.getDefaultUser(), url);

        assertThat(response.getStatusCode(), is(200));
    }

    @Test
    public void verifyDiagnoseApplicationLinkResourceIsPresent() throws IOException, JSONException, ResponseException
    {
        String url = String.format(REST_RESOURCE_URL_FORMAT, TEST_INSTANCE_BASE_URL,  "/" + UUID.randomUUID());
        final HttpClientResponse response = RestHelper.getRestResponse(RestHelper.getDefaultUser(), url);

        assertThat(response.getStatusCode(), is(204));
    }

    @Test
    public void verifyDiagnoseApplicationLinkWithTestResourceIsPresent() throws IOException, JSONException, ResponseException
    {
        String url = String.format(REST_RESOURCE_URL_FORMAT, TEST_INSTANCE_BASE_URL,  "/" + UUID.randomUUID() + "/fake.namespace.Test");
        final HttpClientResponse response = RestHelper.getRestResponse(RestHelper.getDefaultUser(), url);

        assertThat(response.getStatusCode(), is(204));
    }

    @Test
    public void verifyQuickDiagnosisResourceIsPresent() throws IOException, JSONException, ResponseException
    {
        String url = String.format(REST_RESOURCE_URL_FORMAT, TEST_INSTANCE_BASE_URL,  "/quick/" + UUID.randomUUID());
        final HttpClientResponse response = RestHelper.getRestResponse(RestHelper.getDefaultUser(), url);

        assertThat(response.getStatusCode(), is(204));
    }
}