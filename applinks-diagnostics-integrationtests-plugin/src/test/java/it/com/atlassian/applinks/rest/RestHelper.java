package it.com.atlassian.applinks.rest;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.ws.rs.core.MediaType;

import com.atlassian.sal.core.net.HttpClientResponse;
import com.atlassian.user.impl.DefaultUser;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class RestHelper
{
    public static JSONObject getTestCreateAppLinkSubmission(String remoteUrl, DefaultUser user, boolean primary, boolean trustEachOther, boolean sharedUserBase, UUID applicationId, boolean createTwoWayLink)
            throws JSONException
    {
        JSONObject inputAppLink = new JSONObject();
        inputAppLink.put("id", applicationId);
        inputAppLink.put("typeId", "refapp");
        inputAppLink.put("name", remoteUrl);
        inputAppLink.put("rpcUrl", remoteUrl);
        inputAppLink.put("displayUrl", remoteUrl);
        inputAppLink.put("isPrimary", primary);

        JSONObject configFormValuesInput = new JSONObject();
        configFormValuesInput.put("trustEachOther", trustEachOther);
        configFormValuesInput.put("shareUserbase", sharedUserBase);

        JSONObject input = new JSONObject();
        input.put("applicationLink", inputAppLink);
        input.put("username", user.getName());
        input.put("password", user.getPassword());
        input.put("createTwoWayLink", createTwoWayLink);
        input.put("customRpcURL", false);
        input.put("rpcUrl", remoteUrl);
        input.put("configFormValues", configFormValuesInput);
        return input;
    }


    @Nonnull
    public static HttpClientResponse getRestResponse(DefaultUser user, String url)
    {
        return callRestEndPoint(user, new GetMethod(url));
    }

    @Nonnull
    public static HttpClientResponse postRestResponse(DefaultUser user, String url, String requestEntity)
    {
        PostMethod postMethod = new PostMethod(url);
        postMethod.setRequestEntity(new StringRequestEntity(requestEntity));
        postMethod.addRequestHeader("Content-Type", "application/json");
        return callRestEndPoint(user, postMethod);
    }

    @Nonnull
    public static HttpClientResponse putRestResponse(DefaultUser user, String url, String requestEntity)
    {
        PutMethod putMethod = new PutMethod(url);
        if(StringUtils.isNotEmpty(requestEntity))
        {
            putMethod.setRequestEntity(new StringRequestEntity(requestEntity));
        }
        putMethod.addRequestHeader("Content-Type", "application/json");
        return callRestEndPoint(user, putMethod);
    }

    @Nonnull
    public static HttpClientResponse callRestEndPoint(final DefaultUser user, final HttpMethod method)
    {
        org.apache.commons.httpclient.HttpClient httpClient = new org.apache.commons.httpclient.HttpClient();
        method.setRequestHeader("Accept", MediaType.APPLICATION_JSON);

        if (user != null)
        {
            String basicAuth = createAuthorization(user.getName(), user.getPassword());
            method.setRequestHeader("Authorization", "Basic " + basicAuth);
        }

        try
        {
            httpClient.executeMethod(method);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        return new HttpClientResponse(method);
    }

    @Nonnull
    public static String createAuthorization(final String username, final String password)
    {
        return new String(Base64.encodeBase64((username + ":" + password).getBytes()));
    }

    @Nonnull
    public static DefaultUser getDefaultUser()
    {
        DefaultUser defaultUser = new DefaultUser("admin", "admin", "");
        defaultUser.setPassword("admin");
        return defaultUser;
    }

    public static String getLocalHostName()
    {
        try
        {
            return InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException e)
        {
            throw new RuntimeException(e);
        }
    }
}
