package it.com.atlassian.applinks.diagnostics;

import com.atlassian.applinks.pageobjects.AppLinksDiagnosticsPage;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.webdriver.stash.StashTestedProduct;
import com.atlassian.webdriver.stash.page.ProjectListPage;

import org.junit.BeforeClass;
import org.junit.Test;

import it.com.atlassian.applinks.diagnostics.util.StashAdminPage;
import junit.framework.Assert;

import static junit.framework.Assert.assertTrue;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.1
 */

public class DiagnosticsAccessTestAsAdmin
{
    private static final String ADMIN = "admin";

    private static final StashTestedProduct STASH = TestedProductFactory.create(StashTestedProduct.class);

    @BeforeClass
    public static void setup()
    {
        STASH.visit(LoginPage.class).login(ADMIN, ADMIN, ProjectListPage.class);
    }

    @Test
    public void testDiagnosticsLinksIsPresentInAdminPage()
    {
        StashAdminPage stashAdminPage = STASH.visit(StashAdminPage.class);

        PageElement diagnosticsLink = stashAdminPage.getApplicationLinksDiagnosticsLink();
        Assert.assertTrue("Diagnostics link exists", diagnosticsLink.isPresent());
        Assert.assertTrue("Diagnostics link is visible", diagnosticsLink.isVisible());
    }

    @Test
    public void testCanNavigateToAppLinksDiagnosticsPageURL()
    {
        StashAdminPage stashAdminPage = STASH.visit(StashAdminPage.class);
        stashAdminPage.getApplicationLinksDiagnosticsLink().click();

        AppLinksDiagnosticsPage appLinksDiagnosticsPage = new AppLinksDiagnosticsPage();
        assertTrue("Stash is at diagnostics page", STASH.getTester().getDriver().getCurrentUrl().endsWith(appLinksDiagnosticsPage.getUrl()));
    }
}
