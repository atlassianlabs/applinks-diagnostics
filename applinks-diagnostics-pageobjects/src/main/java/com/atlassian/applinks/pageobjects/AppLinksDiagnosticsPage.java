package com.atlassian.applinks.pageobjects;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.google.inject.Inject;

public class AppLinksDiagnosticsPage implements Page
{
    @javax.inject.Inject
    protected PageElementFinder elementFinder;

    @ElementBy(cssSelector = "#applinks-container")
    private PageElement container;

    @Override
    public String getUrl()
    {
        return "/plugins/servlet/applinksDiagnostics/ui";
    }

    public boolean isAt()
    {
        return container.isPresent();
    }
}
