package it.com.atlassian.applinks.diagnostics.util;

import javax.inject.Inject;

import com.atlassian.confluence.pageobjects.page.admin.ConfluenceAdminHomePage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;

import org.openqa.selenium.By;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.1
 */
public class ConfluenceAdminPage extends ConfluenceAdminHomePage
{
    @Inject
    protected PageElementFinder elementFinder;

    public PageElement getApplicationLinksDiagnosticsLink(){
        return elementFinder.find(By.id("diagnose-application-links"));
    }
}
