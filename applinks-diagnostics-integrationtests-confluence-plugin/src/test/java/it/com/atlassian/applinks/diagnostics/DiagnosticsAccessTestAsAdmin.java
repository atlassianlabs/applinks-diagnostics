package it.com.atlassian.applinks.diagnostics;

import com.atlassian.applinks.pageobjects.AppLinksDiagnosticsPage;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractInjectableWebDriverTest;
import com.atlassian.pageobjects.elements.PageElement;

import org.junit.Test;

import it.com.atlassian.applinks.diagnostics.util.ConfluenceAdminPage;
import junit.framework.Assert;

import static junit.framework.Assert.assertTrue;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.1
 */

public class DiagnosticsAccessTestAsAdmin extends AbstractInjectableWebDriverTest
{
    @Test
    public void testDiagnosticsLinksIsPresentInAdminPage()
    {
        ConfluenceAdminPage confluenceAdminPage = product.login(User.ADMIN, ConfluenceAdminPage.class);
        PageElement diagnosticsLink = confluenceAdminPage.getApplicationLinksDiagnosticsLink();
        Assert.assertTrue("Diagnostics link exists", diagnosticsLink.isPresent());
        Assert.assertTrue("Diagnostics link is visible", diagnosticsLink.isVisible());
    }

    @Test
    public void testCanNavigateToAppLinksDiagnosticsPageURL()
    {
        ConfluenceAdminPage confluenceAdminPage = product.login(User.ADMIN, ConfluenceAdminPage.class);
        confluenceAdminPage.getApplicationLinksDiagnosticsLink().click();

        AppLinksDiagnosticsPage appLinksDiagnosticsPage = new AppLinksDiagnosticsPage();
        assertTrue("Confluence is at diagnostics page", product.getTester().getDriver().getCurrentUrl().endsWith(appLinksDiagnosticsPage.getUrl()));
    }
}
